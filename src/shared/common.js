// Global object to attach pieces.
window.tiy = {};

// Language config.
window.tiy.langs = {
  default: 'en',
  // List supported languages here.
  supported: [
    { code: 'en', localName: 'English' },
    { code: 'tr', localName: 'Türkçe' },
  ],
};

// Functions
window.tiy.preloadImage = function (url) {
  new Image().src = 'images/' + url;
};
