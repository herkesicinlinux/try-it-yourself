const stepsObj = window.tiy.messages[window.tiy.lang.code].steps;

// Build steps string.
const buildStepsHTML = function (stepsObj) {
  let stepsHTML = '';
  stepsHTML += `<p id="title">${stepsObj.title}</p>`;
  if (stepsObj.description) stepsHTML += `<div id="description">${stepsObj.description}</div>`;
  stepsHTML += `<div>${stepsObj.subtitle}</div>`;

  stepsHTML += `<${stepsObj.list.type}>`;
  stepsObj.list.items.forEach(function (item) {
    stepsHTML += `<li>${item}</li>`;
  });
  stepsHTML += `</${stepsObj.list.type}>`;

  if (stepsObj.extra) stepsHTML += `<p id="extra">${stepsObj.extra}</p>`;
  return stepsHTML;
};

// Build available langs element.
const buildAvailableLangsEl = function (availableLangs) {
  const baseUrl = `${window.location.origin}${window.location.pathname}`;
  const el = document.createElement('div');
  el.id = 'lang-switch';

  const langLinks = availableLangs.map(function (lang) {
    return `<a href="${baseUrl}?lang=${lang.code}">${lang.localName}</a>`;
  });

  el.innerHTML = langLinks.join(' | ');
  return el;
};

// Write to page.
document.querySelector('#instruction_text').innerHTML = buildStepsHTML(stepsObj);

// Set document title after removing html tags.
document.title = stepsObj.title.replace(/(<([^>]+)>)/gi, '');

// Append language switch element if requested.
window.tiy.langs.allowSwitch &&
  document.body.appendChild(buildAvailableLangsEl(window.tiy.langs.available));

// Set data attribute for theme.
document.querySelector('html').dataset.theme = window.tiy.theme;
