// Get url params.
const urlParams = new URLSearchParams(window.location.search);
const langParam = urlParams.get('lang') ? urlParams.get('lang').trim().toLowerCase() : null;
const langSwitchParam = urlParams.get('langswitch') !== null;
const noFocusParam = urlParams.get('nofocus') !== null;
const themeParam = urlParams.get('theme') ? urlParams.get('theme').trim().toLowerCase() : 'light';

const langsObj = window.tiy.langs;

const matchedLang =
  langParam &&
  langsObj.supported.find(function (lang) {
    return lang.code === langParam;
  });
const defaultLang = langsObj.supported.find(function (lang) {
  return lang.code === langsObj.default;
});

const langObj = matchedLang || defaultLang;

const availableLangs = langsObj.supported.filter(function (lang) {
  return lang.code !== langObj.code;
});

// Attach to the global object.
window.tiy.lang = langObj;
window.tiy.langs.available = availableLangs;
window.tiy.langs.allowSwitch = langSwitchParam;
window.tiy.focus = !noFocusParam;
window.tiy.theme = themeParam;
