/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
*/

const screenImages = [
  'Ubuntu_desktop.jpg',
  'Output_Step_1.jpg',
  'Output_Step_2.jpg',
  'Output_Step_3.jpg',
  'Output_Step_4.jpg',
  'Output_Step_5.jpg',
];

// Preload images.
screenImages.forEach(window.tiy.preloadImage);
