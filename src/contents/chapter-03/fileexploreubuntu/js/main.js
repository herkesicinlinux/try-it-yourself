const messages = window.tiy.messages[window.tiy.lang.code];

let close_flag = '';
let pageCount = 0;
(function () {
  let incorrectFB = '';

  $.fn.initActivity = function () {
    init();
  };

  function init() {
    bindErrorEvent();
    loadPage1();
  }
  function bindErrorEvent() {
    // incorrect feedback event
    $('#incorrect-feedback-wrapper').click(function () {
      incorrectFB = String(messages.feedbacks[pageCount][0]);
      showFeedback(false);
    });
  }

  function loadPage1() {
    pageCount = 0;
    const pageData = '<div class="hotspot" id="page1hotspot_0"></div>';
    populateContent(pageData);
    $('.hotspot').click(function () {
      loadPage2();
    });
  }

  function loadPage2() {
    pageCount = 1;
    const pageData = '<div class="hotspot" id="page2hotspot_0"></div>';
    populateContent(pageData);

    $('.hotspot').click(function () {
      loadPage3();
    });
  }

  function loadPage3() {
    pageCount = 2;
    const pageData = '<div class="hotspot" id="page3hotspot_0"></div>';
    populateContent(pageData);

    $('.hotspot').dblclick(function () {
      loadPage4();
    });
  }

  function loadPage4() {
    pageCount = 3;
    const pageData = '<div class="hotspot" id="page4hotspot_0"></div>';
    populateContent(pageData);

    $('.hotspot').dblclick(function () {
      loadPage5();
    });
  }

  const keyIsDown = {};
  let ctrlDown = false;
  const ctrlKey = 17;
  const vKey = 76;

  function loadPage5() {
    pageCount = 4;
    const pageData = '';
    populateContent(pageData);

    document.onkeydown = overrideKeyboardEvent;
    document.onkeyup = overrideKeyboardEvent;

    $(document).on('keydown', function (e) {
      if (e.keyCode == ctrlKey) ctrlDown = true;
    });

    $(document).on('keyup', function (e) {
      if (e.keyCode == ctrlKey) ctrlDown = false;
    });
  }

  function overrideKeyboardEvent(e) {
    switch (e.type) {
      case 'keydown':
        if (!keyIsDown[e.keyCode]) {
          keyIsDown[e.keyCode] = true;
          // do key down stuff here
        }
        break;
      case 'keyup':
        delete keyIsDown[e.keyCode];
        // do key up stuff here
        if (ctrlDown && e.keyCode == 76) {
          loadPage6();
        }

        break;
    }
    disabledEventPropagation(e);
    e.preventDefault();
    return false;
  }
  function disabledEventPropagation(e) {
    if (e) {
      if (e.stopPropagation) {
        e.stopPropagation();
      } else if (window.event) {
        window.event.cancelBubble = true;
      }
    }
  }

  function loadPage6() {
    pageCount = 5;
    const pageData = '';
    populateContent(pageData);
    showFeedback(true);
  }

  // Show feedback popup
  function showFeedback(flag) {
    $('input').blur();
    let fbStr = '';
    if (flag) {
      // Set correct feedback text and their style;
      fbStr = `<span style="font-size:16px; color:#000">${messages.success[0]}</span>`;
      close_flag = true;
      setTimeout(function () {
        updateFeedbackPosition();
        $('.overlay-bg').show();
      }, 1000);
    } else {
      // Set incorrect feedback text and their style;
      fbStr = `<span style="font-size:16px; color:#3c3c3c"><span>${incorrectFB}</span>`;
      // Set delay to show popup
      setTimeout(function () {
        // display your popup
        updateFeedbackPosition();
        $('.overlay-bg').show();
      }, 100);
    }

    // Put content in feedback popup.
    $('.overlay-content').html(fbStr);
  }

  function populateContent(pageData) {
    $('#activity-hotspot').html(pageData);
    $('#activity-container').css('background-image', `url("images/${screenImages[pageCount]}")`);
    // $('.step_text').html(stepArray[pageCount][0])
  }
  function updateFeedbackPosition() {
    const parentW = $('.overlay-bg').width();
    const parentH = $('.overlay-bg').height();

    const childW = $('.overlay-popup').width();
    const childH = $('.overlay-popup').height();

    const topPos = parentH / 2 - childH / 2;
    const leftPos = parentW / 2 - childW / 2;
    $('.overlay-popup').css({ top: topPos - 30 });
    $('.overlay-popup').css({ left: leftPos });
  }
})(jQuery);
