/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
	use \ (backslash) if apostrop or double quotes comes between command.
*/

/*
0 - code string (Code need to be paste as 1st parameter)
1 - Answer string (which will display next line)  if you have else keep blank it.
2 - to show input box need to pass "write" else keep blank it.
*/
const programmeStr = [
  ['student:/tmp> ', 'file sam1', 'write'],
  ['sam1: a /bin/bash script text executable<br/>student:/tmp> ', 'file new-test1', 'write'],
  ['new-test1: empty<br/>student:/tmp> ', 'file videos', 'write'],
  ['videos: directory<br/>student:/tmp> ', '', ''],
];
