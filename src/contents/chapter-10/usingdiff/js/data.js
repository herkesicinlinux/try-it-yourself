/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
	use \ (backslash) if apostrop or double quotes comes between command.
*/

/*
0 - code string (Code need to be paste as 1st parameter)
1 - Answer string (which will display next line)  if you have else keep blank it.
2 - to show input box need to pass "write" else keep blank it.
*/
const programmeStr = [
  ['student:/tmp> ', 'diff MyFile1 MyFile2', 'write'],
  [
    '1c1<br/>< This data is available in MyFile1<br/>---<br/>> This data is available in MyFile2<br/>student:/tmp> ',
    'diff3 MyFile1 MyFile2 MyFile3',
    'write',
  ],
  [
    '1:1c<br/>This data is available in MyFile1<br/>2:1c<br/>This data is available in MyFile2 <br/>3:1c<br/>This data is available in MyFile3<br/>student:/tmp> ',
    '',
    '',
  ],
];
