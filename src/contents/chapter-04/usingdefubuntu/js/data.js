/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
*/

const screenImages = [
  '0_UbuntuDesktop.jpg',
  '1_LocatingFirefox.jpg',
  '2_UbuntuDesktop.jpg',
  '3_Ubuntu_def_app.jpg',
  '4_Ubuntu_def_app.jpg',
  '5_Ubuntu_def_app.jpg',
  '6_Ubuntu_def_app.jpg',
  '7_Ubuntu_def_app.jpg',
];

// Preload images.
screenImages.forEach(window.tiy.preloadImage);
