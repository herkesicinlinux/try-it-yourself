/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
*/

const screenImages = ['0_ubuntu_desktop.jpg', '1_ubuntu_switchuser.jpg', '2_ubuntu_switchuser.jpg'];

// Preload images.
screenImages.forEach(window.tiy.preloadImage);
