const messages = window.tiy.messages[window.tiy.lang.code];

let close_flag = '';
let pageCount = -1;
(function () {
  let incorrectFB = '';

  $.fn.initActivity = function () {
    init();
  };

  function init() {
    bindErrorEvent();
    loadPage1();
  }
  function bindErrorEvent() {
    // incorrect feedback event
    $('#incorrect-feedback-wrapper').click(function () {
      incorrectFB = String(messages.feedbacks[pageCount][0]);
      showFeedback(false);
    });
  }

  function loadPage1() {
    pageCount++;
    const pageData = '<div class="hotspot" id="page1hotspot"></div>';
    populateScreen(pageData);
    $('.hotspot').click(function () {
      loadPage2();
    });
  }

  function loadPage2() {
    pageCount++;
    const pageData = '<div class="hotspot" id="page2hotspot"></div>';
    populateScreen(pageData);
    $('.hotspot').click(function () {
      loadPage3();
    });
  }

  function loadPage3() {
    pageCount++;
    const pageData =
      '<input id="pg3input" class="input-field" type="password" name="value" value="" maxlength=""/>';
    populateScreen(pageData);
    $('input').eq(0).focus();
    // Event to handle Enter key event.
    $('.input-field').keypress(function (event) {
      const code = event.keyCode || event.which;
      if (code == 13) {
        inputVal = $.trim($(this).val());
        if (inputVal == 'Torv@Ld5') {
          loadEndPage();
        } else {
          $('input').val('');
          incorrectFB = String(messages.feedbacks[pageCount][0]);
          showFeedback(false);
        }
      }
    });
  }

  function loadEndPage() {
    // pageCount++;
    // var pageData=''
    // populateScreen(pageData);
    showFeedback(true);
  }

  // Show feedback popup
  function showFeedback(flag) {
    $('input').blur();
    let fbStr = '';
    if (flag) {
      // Set correct feedback text and their style;
      fbStr = messages.success[0];
      // Put content in feedback popup.
      $('.overlay-content').html(fbStr);
      close_flag = true;
      setTimeout(function () {
        updateFeedbackPosition();
        $('.overlay-bg').show();
      }, 1000);
    } else {
      // Set incorrect feedback text and their style;
      fbStr = incorrectFB;
      // Put content in feedback popup.
      $('.overlay-content').html(fbStr);

      // Set delay to show popup
      setTimeout(function () {
        // display your popup
        updateFeedbackPosition();
        $('.overlay-bg').show();
      }, 100);
    }
  }

  function populateScreen(pageData) {
    $('#activity-hotspot').html(pageData);
    $('#activity-container').css('background-image', `url("images/${screenImages[pageCount]}")`);
    // $('.step_text').html(steps[pageCount][0])
  }
  function updateFeedbackPosition() {
    const parentW = $('.overlay-bg').width();
    const parentH = $('.overlay-bg').height();

    const childW = $('.overlay-popup').width();
    const childH = $('.overlay-popup').height();

    const topPos = parentH / 2 - childH / 2;
    const leftPos = parentW / 2 - childW / 2;
    $('.overlay-popup').css({ top: topPos - 30 });
    $('.overlay-popup').css({ left: leftPos });
  }
})(jQuery);
