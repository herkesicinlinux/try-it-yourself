const messages = window.tiy.messages[window.tiy.lang.code];

let close_flag = '';
let pageCount = -1;
(function () {
  let incorrectFB = '';

  $.fn.initActivity = function () {
    init();
  };

  function init() {
    bindErrorEvent();
    loadPage1();
  }
  function bindErrorEvent() {
    // incorrect feedback event
    $('#incorrect-feedback-wrapper').click(function () {
      incorrectFB = String(messages.feedbacks[pageCount][0]);
      showFeedback(false);
    });
  }

  function loadPage1() {
    pageCount++;
    const pageData = '';
    populateScreen(pageData);
    $('#activity-container').bind('contextmenu', function (e) {
      e.preventDefault();
      loadPage2();
    });
  }

  function loadPage2() {
    $('#activity-container').unbind('contextmenu');
    pageCount++;
    const pageData = '<div class="hotspot" id="page2hotspot"></div>';
    populateScreen(pageData);
    $('.hotspot').click(function () {
      loadPage3();
    });
  }

  function loadPage3() {
    $('#activity-container').unbind('contextmenu');
    pageCount++;
    const pageData = '<div class="hotspot" id="page3hotspot_0"></div>';
    populateScreen(pageData);
    $('.hotspot').click(function () {
      loadPage4();
    });
  }
  function loadPage4() {
    pageCount++;
    const pageData =
      '<div class="hotspot" id="page4hotspot_0"></div><div class="hotspot" id="page4hotspot_1"></div></div><div class="hotspot" id="page4hotspot_4"></div>';
    populateScreen(pageData);
    $('#page4hotspot_1').hide();
    $('#page4hotspot_4').hide();
    let bgSelected = false;
    $('.hotspot').click(function () {
      const btnid = $(this).attr('id').split('_')[1];
      let imgid = '';
      if (btnid == 0 || btnid == 1) {
        $('#page4hotspot_4').show();
        imgid = btnid;
        bgSelected = true;
      }

      if (bgSelected && btnid == 4) {
        if (imgid == 0) {
          $('#activity-container').css(
            'background-image',
            'url("images/4_OpenSUSE_Change_DesktopBck.jpg")'
          );
        } else if (imgid == 1) {
          $('#activity-container').css(
            'background-image',
            'url("images/2_OpenSUSE_Change_DesktopBck.jpg")'
          );
        }
        loadEndPage();
      } else if (!bgSelected) {
        incorrectFB = String(messages.feedbacks[pageCount][0]);
        showFeedback(false);
      }
    });
  }

  function loadEndPage() {
    // pageCount++;
    // var pageData=''
    // populateScreen(pageData);
    $('.hotspot').remove();
    showFeedback(true);
  }

  // Show feedback popup
  function showFeedback(flag) {
    let fbStr = '';
    if (flag) {
      // Set correct feedback text and their style;
      fbStr = messages.success[0];
      // Put content in feedback popup.
      $('.overlay-content').html(fbStr);
      close_flag = true;
      setTimeout(function () {
        updateFeedbackPosition();
        $('.overlay-bg').show();
      }, 1000);
    } else {
      // Set incorrect feedback text and their style;
      fbStr = incorrectFB;
      // Put content in feedback popup.
      $('.overlay-content').html(fbStr);

      // Set delay to show popup
      setTimeout(function () {
        // display your popup
        updateFeedbackPosition();
        $('.overlay-bg').show();
      }, 100);
    }
  }

  function populateScreen(pageData) {
    $('#activity-hotspot').html(pageData);
    $('#activity-container').css('background-image', `url("images/${screenImages[pageCount]}")`);
    // $('.step_text').html(steps[pageCount][0])
  }
  function updateFeedbackPosition() {
    const parentW = $('.overlay-bg').width();
    const parentH = $('.overlay-bg').height();

    const childW = $('.overlay-popup').width();
    const childH = $('.overlay-popup').height();

    const topPos = parentH / 2 - childH / 2;
    const leftPos = parentW / 2 - childW / 2;
    $('.overlay-popup').css({ top: topPos - 30 });
    $('.overlay-popup').css({ left: leftPos });
  }
})(jQuery);
