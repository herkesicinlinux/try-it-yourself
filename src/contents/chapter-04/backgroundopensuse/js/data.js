/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
*/

const screenImages = [
  '0_OpenSUSE_Desktop.jpg',
  '1_OpenSUSE_Change_DesktopBck.jpg',
  '2_OpenSUSE_Change_DesktopBck.jpg',
  '3_OpenSUSE_Change_DesktopBck.jpg',
  '4_OpenSUSE_Change_DesktopBck.jpg',
];

// Preload images.
screenImages.forEach(window.tiy.preloadImage);
