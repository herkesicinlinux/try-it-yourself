/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
*/

const screenImages = [
  '0_OpenSUSEDesktop.jpg',
  '1_OpenSUSE_locating_app.jpg',
  '2_OpenSUSE_locating_app.jpg',
  '3_OpenSUSEDesktop.jpg',
  '4_OpenSUSE_default_app.jpg',
  '5_OpenSUSE_default_app.jpg',
  '6_OpenSUSE_default_app.jpg',
  '7_OpenSUSE_default_app.jpg',
  '8_OpenSUSE_default_app.jpg',
  '9_OpenSUSE_default_app.jpg',
];

// Preload images.
screenImages.forEach(window.tiy.preloadImage);
