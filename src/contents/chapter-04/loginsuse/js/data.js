/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
*/

const screenImages = [
  '0_OpenSUSE_login.jpg',
  '1_OpenSUSE_login.jpg',
  '2_OpenSUSE_desktop.jpg',
  '3_OpenSUSE_logout.jpg',
  '4_OpenSUSE_logout.jpg',
  '5_OpenSUSE_logout.jpg',
];

// Preload images.
screenImages.forEach(window.tiy.preloadImage);
