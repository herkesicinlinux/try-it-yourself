const messages = window.tiy.messages[window.tiy.lang.code];

let close_flag = '';
let pageCount = -1;
(function () {
  let incorrectFB = '';

  $.fn.initActivity = function () {
    init();
  };

  function init() {
    bindErrorEvent();
    loadPage1();
  }
  function bindErrorEvent() {
    // incorrect feedback event
    $('#incorrect-feedback-wrapper').click(function () {
      incorrectFB = String(messages.feedbacks[pageCount][0]);
      showFeedback(false);
    });
  }

  function loadPage1() {
    pageCount++;
    const pageData = '<div class="hotspot" id="page1hotspot"></div>';
    populateScreen(pageData);
    $('.hotspot').click(function () {
      loadPage2();
    });
  }

  function loadPage2() {
    pageCount++;
    const pageData =
      '<div class="hotspot" id="page2hotspot"></div><input id="pg2input" class="input-field" type="password" name="value" value="" maxlength=""/>';
    populateScreen(pageData);
    $('input').eq(0).focus();
    // Event to handle Enter key event.
    $('.input-field').keypress(function (event) {
      const code = event.keyCode || event.which;
      if (code == 13) {
        inputVal = $.trim($(this).val());
        if (inputVal == 'Torv@Ld5') {
          const fbStr = messages.success[0];
          showFeedback(true, fbStr);
          loadPage3();
        } else {
          $('input').val('');
          incorrectFB = String(messages.feedbacks[pageCount][0]);
          showFeedback(false);
        }
      }
    });

    $('.hotspot').click(function () {
      if ($.trim($('#pg2input').val()).length > 0 && $('#pg2input').val() == 'Torv@Ld5') {
        const fbStr = messages.success[0];
        showFeedback(true, fbStr);
        loadPage3();
      } else {
        incorrectFB = String(messages.feedbacks[pageCount][0]);
        showFeedback(false);
      }
    });
  }

  function loadPage3() {
    pageCount++;
    const pageData = '<div class="hotspot" id="page3hotspot"></div>';
    populateScreen(pageData);
    $('.hotspot').click(function () {
      loadPage4();
    });
  }

  function loadPage4() {
    pageCount++;
    const pageData = '<div class="hotspot" id="page4hotspot"></div>';
    populateScreen(pageData);
    $('.hotspot').click(function () {
      loadPage5();
    });
  }
  function loadPage5() {
    pageCount++;
    const pageData = '<div class="hotspot" id="page5hotspot"></div>';
    populateScreen(pageData);
    $('.hotspot').click(function () {
      loadPage6();
    });
  }

  function loadPage6() {
    pageCount++;
    const pageData = '<div class="hotspot" id="page6hotspot"></div>';
    populateScreen(pageData);
    $('.hotspot').click(function () {
      loadEndPage();
    });
  }

  function loadEndPage() {
    pageCount++;
    const pageData = '';
    // populateScreen(pageData);
    close_flag = true;
    const fbStr = messages.success[1];
    showFeedback(true, fbStr);
  }

  // Show feedback popup
  function showFeedback(flag, fbStr) {
    $('input').blur();
    if (flag) {
      // Put content in feedback popup.
      $('.overlay-content').html(fbStr);
      setTimeout(function () {
        updateFeedbackPosition();
        $('.overlay-bg').show();
      }, 1000);
    } else {
      // Set incorrect feedback text and their style;
      fbStr = incorrectFB;
      // Put content in feedback popup.
      $('.overlay-content').html(fbStr);

      // Set delay to show popup
      setTimeout(function () {
        // display your popup
        updateFeedbackPosition();
        $('.overlay-bg').show();
      }, 100);
    }
  }

  function populateScreen(pageData) {
    $('#activity-hotspot').html(pageData);
    $('#activity-container').css('background-image', `url("images/${screenImages[pageCount]}")`);
    // $('.step_text').html(steps[pageCount][0])
  }
  function updateFeedbackPosition() {
    const parentW = $('.overlay-bg').width();
    const parentH = $('.overlay-bg').height();

    const childW = $('.overlay-popup').width();
    const childH = $('.overlay-popup').height();

    const topPos = parentH / 2 - childH / 2;
    const leftPos = parentW / 2 - childW / 2;
    $('.overlay-popup').css({ top: topPos - 30 });
    $('.overlay-popup').css({ left: leftPos });
  }
})(jQuery);
