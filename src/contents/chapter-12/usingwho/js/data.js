/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
	use \ (backslash) if apostrop or double quotes comes between command.
*/

/*
0 - code string (Code need to be paste as 1st parameter)
1 - Answer string (which will display next line)  if you have else keep blank it.
2 - to show input box need to pass "write" else keep blank it.
*/
const programmeStr = [
  ['student:/tmp> ', 'who', 'write'],
  [
    '<table border="0" width="100%"><tr><td>student</td><td>tty1</td><td>2014-06-04 15:50</td><td>(:0)</td></tr><tr><td>student</td><td>pts/0</td><td>2014-06-04 15:55</td><td>(:0.0)</td></tr></table>student:/tmp> ',
    'whoami',
    'write',
  ],
  ['student<br/>student:/tmp> ', '', ''],
];
