/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
	use \ (backslash) if apostrop or double quotes comes between command.
*/

/*
0 - code string (Code need to be paste as 1st parameter)
1 - Answer string (which will display next line)  if you have else keep blank it.
2 - to show input box need to pass "write" else keep blank it.
*/
const programmeStr = [
  ['student:/home/student> ', 'ls -l', 'write'],
  [
    'total 40<br/><table width="100%"><tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:42</td> <td>Desktop</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Documents</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Downloads</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Music</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Pictures</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Public</td> </tr> <tr> <td>-rw-rw-r--.</td> <td>1</td> <td>student</td> <td>student</td> <td>376</td> <td>Jul 24 02:14</td> <td>sample2.sh</td> </tr> <tr> <td>-rw-rw-r--.</td> <td>1</td> <td>student</td> <td>student</td> <td>1453</td> <td>Jul 24 02:13</td> <td>script1.sh</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Templates</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Videos</td> </tr></table>student:/home/student> ',
    'chmod uo+x,g-w sample2.sh',
    'write',
  ],
  ['student:/home/studen> ', 'ls -l', 'write'],
  [
    'total 40<br/><table width="100%"><tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:42</td> <td>Desktop</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Documents</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Downloads</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Music</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Pictures</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Public</td> </tr> <tr> <td>-rwxr--r-x.</td> <td>1</td> <td>student</td> <td>student</td> <td>376</td> <td>Jul 24 02:14</td> <td>sample2.sh</td> </tr> <tr> <td>-rw-rw-r--.</td> <td>1</td> <td>student</td> <td>student</td> <td>1453</td> <td>Jul 24 02:13</td> <td>script1.sh</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Templates</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Videos</td> </tr></table>student@ubuntu:~ ',
    'chmod 751 script1.sh',
    'write',
  ],
  ['student:/home/student> ', 'ls -l', 'write'],
  [
    'total 40<br/><table width="100%"><tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:42</td> <td>Desktop</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Documents</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Downloads</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Music</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Pictures</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Public</td> </tr> <tr> <td>-rwxr--r-x.</td> <td>1</td> <td>student</td> <td>student</td> <td>376</td> <td>Jul 24 02:14</td> <td>sample2.sh</td> </tr> <tr> <td>-rwxr-x--x.</td> <td>1</td> <td>student</td> <td>student</td> <td>1453</td> <td>Jul 24 02:13</td> <td>script1.sh</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Templates</td> </tr> <tr> <td>drwxr-xr-x.</td> <td>2</td> <td>student</td> <td>student</td> <td>4096</td> <td>Jul 23 22:41</td> <td>Videos</td> </tr></table>student:/home/student> ',
    '',
    '',
  ],
];
