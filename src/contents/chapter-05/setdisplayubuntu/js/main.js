const messages = window.tiy.messages[window.tiy.lang.code];

let close_flag = '';
let pageCount = -1;
(function () {
  let incorrectFB = '';

  $.fn.initActivity = function () {
    init();
  };

  function init() {
    bindErrorEvent();
    loadPage1();
  }
  function bindErrorEvent() {
    // incorrect feedback event
    $('#incorrect-feedback-wrapper').click(function () {
      incorrectFB = String(messages.feedbacks[pageCount][0]);
      showFeedback(false);
    });
  }

  function loadPage1() {
    pageCount++;
    const pageData = '<div class="hotspot" id="page1hotspot"></div>';
    populateScreen(pageData);
    $('.hotspot').click(function () {
      loadPage2();
    });
  }

  function loadPage2() {
    pageCount++;
    const pageData = '<div class="hotspot" id="page2hotspot"></div>';
    populateScreen(pageData);
    $('.hotspot').click(function () {
      loadPage3();
    });
  }

  function loadPage3() {
    pageCount++;
    const pageData = '<div class="hotspot" id="page3hotspot"></div>';
    populateScreen(pageData);
    $('.hotspot').click(function () {
      loadPage4();
    });
  }

  function loadPage4() {
    pageCount++;
    const pageData = '<div class="hotspot" id="page4hotspot"></div>';
    populateScreen(pageData);
    $('.hotspot').click(function () {
      loadPage5();
    });
  }

  function loadPage5() {
    pageCount++;
    const pageData = '<div class="hotspot" id="page5hotspot"></div>';
    populateScreen(pageData);
    $('.hotspot').click(function () {
      loadEndPage();
    });
  }

  function loadEndPage() {
    // pageCount++;
    // var pageData=''
    // populateScreen(pageData);
    close_flag = true;
    const fbcorrect = messages.success[0];
    showFeedback(true, fbcorrect);
  }

  // Show feedback popup
  function showFeedback(flag, fbtext) {
    $('input').blur();
    if (flag) {
      // Put content in feedback popup.
      $('.overlay-content').html(fbtext);

      setTimeout(function () {
        updateFeedbackPosition();
        $('.overlay-bg').show();
      }, 1000);
    } else {
      // Put content in feedback popup.
      $('.overlay-content').html(incorrectFB);
      // Set delay to show popup
      setTimeout(function () {
        // display your popup
        updateFeedbackPosition();
        $('.overlay-bg').show();
      }, 100);
    }
  }

  function populateScreen(pageData) {
    $('#activity-hotspot').html(pageData);
    $('#activity-container').css('background-image', `url("images/${screenImages[pageCount]}")`);
    // $('.step_text').html(steps[pageCount][0])
  }
  function updateFeedbackPosition() {
    const parentW = $('.overlay-bg').width();
    const parentH = $('.overlay-bg').height();

    const childW = $('.overlay-popup').width();
    const childH = $('.overlay-popup').height();

    const topPos = parentH / 2 - childH / 2;
    const leftPos = parentW / 2 - childW / 2;
    $('.overlay-popup').css({ top: topPos - 30 });
    $('.overlay-popup').css({ left: leftPos });
  }
})(jQuery);
