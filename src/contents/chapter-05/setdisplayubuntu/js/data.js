/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
*/

const screenImages = [
  '0_Ubuntu.jpg',
  '0_Ubuntu_SettingsWindow.jpg',
  '1_Ubuntu_Display_res.jpg',
  '2_Ubuntu_Display_res.jpg',
  '3_Ubuntu_Display_res.jpg',
];

// Preload images.
screenImages.forEach(window.tiy.preloadImage);
