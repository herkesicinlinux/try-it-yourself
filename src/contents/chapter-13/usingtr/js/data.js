/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
	use \ (backslash) if apostrop or double quotes comes between command.
*/

/*
0 - code string (Code need to be paste as 1st parameter)
1 - Answer string (which will display next line)  if you have else keep blank it.
2 - to show input box need to pass "write" else keep blank it.
*/
const programmeStr = [
  ['student:/tmp> ', 'cat cities', 'write'],
  [
    'Chicago<br/>Madison<br/>Seattle<br/>Boston<br/>student:/tmp> ',
    'cat cities | tr a-z A-Z',
    'write',
  ],
  ['CHICAGO<BR/>MADISON<BR/>SEATTLE<BR/>BOSTON<BR/>student:/tmp> ', '', ''],
];
