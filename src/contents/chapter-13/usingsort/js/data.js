/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
	use \ (backslash) if apostrop or double quotes comes between command.
*/

/*
0 - code string (Code need to be paste as 1st parameter)
1 - Answer string (which will display next line)  if you have else keep blank it.
2 - to show input box need to pass "write" else keep blank it.
*/
const programmeStr = [
  ['student:/tmp> ', 'cat planets', 'write'],
  [
    'Mercury<br/>Mars<br/>Earth<br/>Neptune<br/>Earth<br/>Venus<br/>Mercury<br/>student:/tmp> ',
    'sort planets',
    'write',
  ],
  [
    'Earth<br/>Earth<br/>Mars<br/>Mercury<br/>Mercury<br/>Neptune<br/>Venus<br/>student:/tmp> ',
    'sort planets | uniq',
    'write',
  ],
  ['Earth<br/>Mars<br/>Mercury<br/>Neptune<br/>Venus<br/>student:/tmp> ', '', ''],
];
