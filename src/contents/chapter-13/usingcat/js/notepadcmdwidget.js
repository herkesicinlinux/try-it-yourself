const messages = window.tiy.messages[window.tiy.lang.code];
const focusAllowed = window.tiy.focus;

// Append feedback messages to programmeStr.
const feedbacks = programmeStr.map(function (pArr, i) {
  return pArr.concat(messages.feedbacks[i]);
});

let close_flag = '';
(function () {
  let lines;
  let count = 0;
  let codeTag = '';
  let inputVal = '';
  let incorrectFB = '';
  //
  $.fn.NotepadWidget = function () {
    init();
  };

  function init() {
    $('#activity').empty();
    lines = feedbacks.length;
    populateInputs();
  }

  function populateInputs() {
    let inputTag = '';
    let w = 0;

    codeTag = `<div><span style="" id="span_${count}" class="cmdtext" >${String(
      feedbacks[count][0]
    )}</span>`;
    let has_textarea = false;
    if (String(feedbacks[count][2]) == 'write' || String(feedbacks[count][2]) == 'write_n_Ctrl_D') {
      if (String(feedbacks[count][4]) == 'password') {
        inputTag = `${codeTag}<input id=${count} class="userInput" type="password" name="value" value="" maxlength=""/></div>`;
      } else if (String(feedbacks[count][4]) == 'paragraph') {
        has_textarea = true;
        inputTag = `${codeTag}<textarea id="tarea" cols="80" rows="3" class="userInput"></textarea></div>`;
      } else {
        inputTag = `${codeTag}<input id=${count} class="userInput" type="text" name="value" value="" maxlength="" /></div>`;
      }
    } else {
      inputTag = `${codeTag}</div>`;
    }

    $(inputTag).appendTo($('#activity'));
    if (has_textarea) {
      $('.highlight').appendTo($('#activity'));
    }
    if (String(feedbacks[count][2]) == 'write') {
      w = $('input').width() - $('#span_0').width();
      // $('input').css('width', `${w}px`);
      if (count == 0 && focusAllowed) {
        $('input').eq(0).focus();
      }
      bindInputEvent();
    }

    if (String(feedbacks[count][2]) == 'write_n_Ctrl_D') {
      $('textarea').attr('disabled', true);
      if (count == 0 && focusAllowed) {
        $('input').eq(0).focus();
      }
      bindInputEvent_Ctrl_D();
    }

    // It will be last statement of the programme to display on screen.
    if (String(feedbacks[count][2]) == 'last') {
      $('#activity').animate({ scrollTop: 65000 }, 1500);

      setTimeout(function () {
        const fbtext = messages.success[0];
        close_flag = true;
        showFeedback(true, fbtext);
      }, 1500);
    }
  }
  function has_correct_fb() {
    return feedbacks[count][5] != undefined && String(feedbacks[count][5]) != '';
  }
  function bindInputEvent_Ctrl_D() {
    $('.highlight').removeClass('hidden');
    // $('textarea').eq(0).focus();
    $('.userInput').keydown(function (event) {
      if (event.ctrlKey && (event.which == 68 || event.which == 100)) {
        event.preventDefault();
        event.stopImmediatePropagation();
        event.stopPropagation();

        const text = $('.userInput').val();
        const lines = text.split(/\r|\r\n|\n/);
        const linecount = lines.length;
        console.log(linecount); //
        // alert('$(this).val() ' + linecount);
        if (linecount < 2) {
          incorrectFB = String(feedbacks[count][3]);
          showFeedback(false, incorrectFB);
          return;
        }

        inputVal = $.trim($(this).val());
        correctVal = String(feedbacks[count][1]);
        // var correctValArr = String(correctVal).split('~');
        let val1 = '';
        const rexp = new RegExp(/\n/g);
        inputVal = inputVal.replace(rexp, '<br/>');
        if ($('.textarea').length) {
          if ($('textarea').val().toLowerCase().indexOf('\n') == -1) {
            $(this).css({ border: '0px solid red' });
            $('textarea').val('');
            $('textarea').css('color', '#fff');
            incorrectFB = String(feedbacks[count][3]);
            showFeedback(false, incorrectFB);

            return;
          }
        }
        if (inputVal == correctVal) {
          val1 = $(`#span_${count}`).html();

          if (String(feedbacks[count][4]) == 'password') {
            $(`#span_${count}`).html(val1);
          } else {
            $(`#span_${count}`).html(val1 + inputVal);
          }

          $(this).remove();

          if (has_correct_fb()) {
            show_correct_fb();
          }
          count++;
          populateInputs();
        } else {
          $(this).css({ border: '0px solid red' });
          $('textarea').val('');
          $('textarea').css('color', '#fff');
          incorrectFB = String(feedbacks[count][3]);
          showFeedback(false, incorrectFB);
        }
      }
    });
  }
  function bindInputEvent() {
    $('.highlight').removeClass('hidden').addClass('hidden');
    // $('input').eq(0).focus();
    // Event to handle Enter key event.
    $('.userInput').keypress(function (event) {
      if (String(feedbacks[count][4]) == 'password') {
        $('input').css('color', '#fff');
      }
      if (event.keyCode == 13) {
        inputVal = $.trim($(this).val());
        correctVal = String(feedbacks[count][1]);
        const correctValArr = String(correctVal).split('~');
        let val1 = '';

        if (inputVal == correctValArr[0] || inputVal == correctValArr[1]) {
          val1 = $(`#span_${count}`).html();

          if (String(feedbacks[count][4]) == 'password') {
            $(`#span_${count}`).html(val1);
          } else {
            $(`#span_${count}`).html(val1 + inputVal);
          }

          $(this).remove();

          // show feedback on completed 1st command.
          if (String(feedbacks[count][4]).length > 0) {
            showFeedback(true, feedbacks[count][4]);
          }

          if (has_correct_fb()) {
            show_correct_fb();
          }

          count++;
          populateInputs();
        } else {
          $(this).css({ border: '0px solid red' });
          $('input').val('');
          $('input').css('color', '#fff');

          incorrectFB = String(feedbacks[count][3]);
          showFeedback(false, incorrectFB);
        }
      }
    });
  }

  // Show feedback popup
  // Show feedback popup
  function showFeedback(flag, fbStr) {
    isPopupOpen = true;
    $('input').blur();
    $('textarea').blur();
    if (flag) {
      // Set correct feedback text and their style;
      updateFeedbackPosition();
      $('.overlay-bg').show();
    } else {
      // Set incorrect feedback text and their style;

      // Set delay to show popup
      setTimeout(function () {
        // display your popup
        updateFeedbackPosition();
        $('.overlay-bg').show();
      }, 100);
    }

    // Put content in feedback popup.
    $('.overlay-content').html(fbStr);
  }

  function show_correct_fb() {
    $('input').blur();
    $('textarea').blur();
    const fbStr = `<span style="font-size:16px; color:#111111"><span>${feedbacks[count][5]}</span>`;
    // Set delay to show popup
    setTimeout(function () {
      // display your popup
      updateFeedbackPosition();
      $('.overlay-bg').show();
    }, 800);
    // Put content in feedback popup.
    $('.overlay-content').html(fbStr);
  }

  function updateFeedbackPosition() {
    const parentW = $('.overlay-bg').width();
    const parentH = $('.overlay-bg').height();

    const childW = $('.overlay-popup').width();
    const childH = $('.overlay-popup').height();

    const topPos = parentH / 2 - childH / 2;
    const leftPos = parentW / 2 - childW / 2;
    $('.overlay-popup').css({ top: topPos - 30 });
    $('.overlay-popup').css({ left: leftPos });
  }
})(jQuery);
