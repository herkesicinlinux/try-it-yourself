$(document).ready(function () {
  $(document).keyup(function (e) {
    if (e.keyCode == 27) {
      isPopupOpen = false;
      $('.close-btn').trigger('click');
      /* $('.overlay-bg').hide();
		  $('input:visible:enabled:first').focus();

		 // alert('programmeStr count ' + count);
		 if(count == programmeStr.length-1){
			closePopup();
		 } */
    } // esc
  });
  // hide popup when user clicks on close button
  $('.close-btn').click(function () {
    if (close_flag) {
      window.close();
    }
    if ($('textarea').length > 0) {
      $('textarea').focus();
      $('textarea').attr('disabled', false);
      $('textarea').val('');
      $('textarea').select();
    } else {
      $('input').focus();
      $('input').select();
    }
    $('.overlay-bg').hide(); // hide the overlay
  });

  // hides the popup if user clicks anywhere outside the container
  $('.overlay-bg').click(function () {
    // $('.overlay-bg').hide();
  });
  // prevents the overlay from closing if user clicks inside the popup overlay
  $('.overlay-content').click(function () {
    return false;
  });
});
