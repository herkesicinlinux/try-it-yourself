/*
0 - code string (Code need to be paste as 1st parameter)
1 - Answer string (which will display next line)  if you have else keep blank it.
2 - to show input box need to pass "write" else keep blank it.
*/

/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
*/
const programmeStr = [
  ['student:/tmp> ', 'whereis gcc', 'write'],
  [
    'gcc: /usr/bin/gcc /usr/lib/gcc /usr/libexec/gcc /usr/lib64/ccache/gcc /usr/share/man/man1/gcc.1.gz<br/>student:/tmp> ',
    'pwd',
    'write',
  ],
  ['student:/tmp><br/>student:/tmp> ', 'cd /usr/bin', 'write'],
  ['student:/usr/bin> ', 'cd', 'write'],
  ['student:/home/student> ', 'cd ..', 'write'],
  ['student:/home> ', 'cd -', 'write'],
  ['/home/student<br/>student:/home/student> ', 'pwd', 'write'],
  ['/home/student<br/>student:/home/student> ', ''],
];
