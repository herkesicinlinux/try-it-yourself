/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
*/

/*
0 - code string (Code need to be paste as 1st parameter)
1 - Answer string (which will display next line)  if you have else keep blank it.
2 - to show input box need to pass "write" else keep blank it.
*/
const programmeStr = [
  ['student:/tmp> ', 'locate .doc', 'write'],
  [
    '/usr/lib/python2.7/pdb.doc<br/>student:/tmp> ',
    'cp /usr/lib/python2.7/pdb.doc Myfile.doc',
    'write',
  ],
  ['student:/tmp> ', 'sudo updatedb', 'write'],
  ['student:/tmp> ', 'locate Myfile.doc', 'write'],
  ['/home/student/Myfile.doc<br/>student:/tmp> ', '', ''],
];
