/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
*/

/*
0 - code string (Code need to be paste as 1st parameter)
1 - Answer string (which will display next line)  if you have else keep blank it.
2 - to show input box need to pass "write" else keep blank it.
*/
const programmeStr = [
  ['student:/tmp> ', 'find /usr -name gcc', 'write'],
  [
    "/usr/bin/gcc<br/>/usr/libexec/gcc<br/>find: `/usr/lib64/audit': Permission denied<br/>/usr/lib/gcc<br/>student:/tmp> ",
    'find /usr -type d -name gcc',
    'write',
  ],
  [
    "/usr/libexec/gcc<br/>find: `/usr/lib64/audit': Permission denied<br/>/usr/lib/gcc<br/>student:/tmp> ",
    'find -type f -mtime 0',
    'write',
  ],
  [
    './.ICEauthority<br/>./.esd_auth<br/>./.gstreamer-0.10/registry.x86_64.bin<br/>./.gconfd/saved_state<br/>./.local/share/applications/preferred-mail-reader.desktop<br/>./.local/share/applications/preferred-web-browser.desktop<br/>./.local/share/.converted-launchers<br/>student:/tmp> ',
    'find -type f -size 0',
    'write',
  ],
  [
    './.local/share/.converted-launchers<br/> ./.gnupg/pubring.gpg<br/>./.gnupg/secring.gpg<br/>./.gconf/desktop/gnome/accessibility/%gconf.xml<br/>./.gconf/desktop/gnome/%gconf.xml<br/> ./.gconf/desktop/%gconf.xml<br/> ./.gconf/apps/panel/%gconf.xml<br/> ./.gconf/apps/panel/applets/clock/%gconf.xml<br/> ./.gconf/apps/panel/applets/workspace_switcher/%gconf.xml<br/> ./.gconf/apps/panel/applets/%gconf.xml<br/> ./.gconf/apps/panel/applets/window_list/%gconf.xml<br/> ./.gconf/apps/nautilus/desktop-metadata/%gconf.xml<br/> ./.gconf/apps/nautilus/%gconf.xml<br/> ./.gconf/apps/brasero/%gconf.xml<br/> ./.gconf/apps/%gconf.xml<br/> ./.gconf/apps/gnome-terminal/%gconf.xml<br/> ./.gconf/apps/gnome-terminal/profiles/%gconf.xml<br/> student:/tmp> ',
    '',
    '',
  ],
];
