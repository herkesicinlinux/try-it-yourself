/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
*/

/*
0 - code string (Code need to be paste as 1st parameter)
1 - Answer string (which will display next line)  if you have else keep blank it.
2 - to show input box need to pass "write" else keep blank it.
*/
const programmeStr = [
  ['student:/tmp>   ', 'touch -t 1803141400 file1 file2', 'write'],
  ['student:/tmp>   ', 'ls -l file1 file2', 'write'],
  [
    '<table width="100%"><tr> <td>-rw-r--r--.</td> <td>1</td> <td>file1</td> <td>file1</td> <td>0</td> <td>Mar 14  2018</td> <td>file1</td> </tr> <tr> <td>-rw-r--r--.</td> <td>1</td> <td>file1</td> <td>file1</td> <td>0</td> <td>Mar 14  2018</td> <td>file2</td> </tr></table>student:/tmp>   ',
    'mv file1 new_file1',
    'write',
  ],
  ['student:/tmp>   ', 'rm file2', 'write'],
  ['student:/tmp>   ', 'rm new_file1', 'write'],
  ['student:/tmp>   ', 'mkdir dir1', 'write'],
  ['student:/tmp>   ', 'rmdir dir1', 'write'],
  ['student:/tmp>   ', ''],
];
