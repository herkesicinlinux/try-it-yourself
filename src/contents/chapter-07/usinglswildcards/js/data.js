/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
*/

/*
0 - code string (Code need to be paste as 1st parameter)
1 - Answer string (which will display next line)  if you have else keep blank it.
2 - to show input box need to pass "write" else keep blank it.
*/
const programmeStr = [
  ['student:/tmp/play> ', 'ls -a', 'write'],
  [
    '<table border="0" width="100%"><tr><td>.</td><td>gindxbib</td><td>mkfontscale</td></tr><tr><td>..</td><td>gio-querymodules-64</td><td>mkhybrid</td></tr><tr><td>[</td><td>glib-compile-schemas</td><td>mkisofs</td></tr><tr><td>a2p</td><td>glookbib</td><td>mkmanifest</td></tr><tr><td>ab</td><td>glxgears</td><td>mkrfc2734</td></tr><tr><td>abrt-action-analyze-backtrace</td><td>glxinfo</td><td>mkxauth</td></tr><tr><td>abrt-action-analyze-c</td><td>gmake</td><td>mlabel</td></tr><tr><td>abrt-action-analyze-ccpp-local</td><td>gneqn</td><td>mmc-tool</td></tr></table>student:/tmp/play> ',
    'ls g????',
    'write',
  ],
  ['gmake gneqn <br/>student:/tmp/play> ', 'ls mk*', 'write'],
  [
    '<table width="100%"><tr><td>mkfontscale</td><td>mkhybrid</td><td>mkisofs</td><td>mkmanifest</td><td>mkrfc2734</td><td>mkxauth</td></tr></table>student:/tmp/play> ',
    'ls g[a-n]???',
    'write',
  ],
  ['gmake gneqn <br/>student:/tmp/play> ', 'ls g[!a-m]???', 'write'],
  ['gneqn<br/>student:/tmp/play> ', '', ''],
];
