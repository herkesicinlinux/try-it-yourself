/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
*/

/*
0 - code string (Code need to be paste as 1st parameter)
1 - Answer string (which will display next line)  if you have else keep blank it.
2 - to show input box need to pass "write" else keep blank it.
*/
const programmeStr = [
  ['student:/tmp> ', 'sudo ethtool eth0', 'write'],
  ['[sudo] password for test1: ', 'student', 'write'],
  [
    "Settings for eth0:<table width='590px' border='0'><tr><td width='40'></td><td>Supported ports:[TP]</td><td></td></tr><tr><td></td><td>Supported link modes:</td><td>10baseT/Half 10baseT/Full</td></tr><tr><td></td><td></td><td>100baseT/Half 100baseT/Full </td></tr><tr><td></td><td></td><td>1000baseT/Full</td></tr><tr><td></td><td>Supported pause frame use: No</td><td></td></tr><tr><td></td><td>Supports auto-negotiation: Yes</td><td></td></tr><tr><td></td><td>Advertised link modes:  </td><td>10baseT/Half 10baseT/Full </td></tr><tr><td></td><td>Advertised link modes:  </td><td>10baseT/Half 10baseT/Full </td></tr><tr><td></td><td></td><td>100baseT/Half 100baseT/Full</td></tr><tr><td></td><td></td><td>1000baseT/Full </td></tr><tr><td></td><td>Advertised pause frame use: No</td><td></td></tr><tr><td></td><td>Advertised auto-negotiation: Yes</td><td></td></tr><tr><td></td><td>Speed: 1000Mb/s</td><td></td></tr><tr><td></td><td>Duplex: Full</td><td></td></tr><tr><td></td><td>Port: Twisted Pair</td><td></td></tr><tr><td></td><td>PHYAD: 0</td><td></td></tr><tr><td></td><td>Transceiver: internal</td><td></td></tr><tr><td></td><td>Auto-negotiation: on</td><td></td></tr><tr><td></td><td>MDI-X: off (auto)</td><td></td></tr><tr><td></td><td>Supports Wake-on: umbg</td><td></td></tr><tr><td></td><td>Wake-on: d</td><td></td></tr><tr><td></td><td>Current message level: 0x00000007 (7)</td><td></td></tr><tr><td></td><td></td><td>drv probe link</td></tr><tr><td></td><td>Link detected: yes</td><td></td></tr></table>student:/tmp> ",
    'netstat -r',
    'write',
  ],
  [
    '<table><tr><td width="120">Destination</td><td width="120">Gateway</td><td width="120">Genmask</td><td width="70">Flags</td><td width="60">Metric</td><td width="60">Ref</td><td width="60">Use</td><td width="60">Iface</td></tr><tr><td width="120">default</td><td width="120">10.0.2.2</td><td width="120">0.0.0.0 </td><td width="70">UG</td><td width="60">0</td><td width="60">0</td><td width="60">0</td><td width="60">eth0</td></tr><tr><td width="100">10.0.2.0</td><td width="100">*</td><td width="100">255.255.255.0</td><td width="70">U</td><td width="60">0</td><td width="60">0</td><td width="60">0</td><td width="60">eth0</td></tr></table>student:/tmp> ',
    '',
    '',
  ],
];
