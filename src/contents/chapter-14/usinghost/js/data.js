/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;

	You have successfully completed the assigned task. You will now lookup information about the domain name of linuxfoundation.org
*/

/*
0 - code string (Code need to be paste as 1st parameter)
1 - Answer string (which will display next line)  if you have else keep blank it.
2 - to show input box need to pass "write" else keep blank it.
*/
const programmeStr = [
  ['student:/tmp>  ', 'cat /etc/hosts', 'write'],
  [
    '127.0.0.1	localhost<br/>127.0.1.1	ubuntu<br/># The following lines are desirable for IPv6 capable hosts<br/>::1     ip6-localhost ip6-loopback<br/>fe00::0 ip6-localnet<br/>ff00::0 ip6-mcastprefix<br/>ff02::1 ip6-allnodes<br/>ff02::2 ip6-allrouters<br/>student:/tmp>  ',
    'host linuxfoundation.org',
    'write',
  ],
  [
    'linuxfoundation.org has address 140.211.169.4<br/>linuxfoundation.org mail is handled by 5 ALT1.ASPMX.L.GOOGLE.COM.<br/>linuxfoundation.org mail is handled by 5 ALT2.ASPMX.L.GOOGLE.COM.<br/>linuxfoundation.org mail is handled by 10 ASPMX2.GOOGLEMAIL.COM.<br/>linuxfoundation.org mail is handled by 10 ASPMX3.GOOGLEMAIL.COM.<br/>linuxfoundation.org mail is handled by 1 ASPMX.L.GOOGLE.COM.<br/>student:/tmp>  ',
    '',
    '',
  ],
];
