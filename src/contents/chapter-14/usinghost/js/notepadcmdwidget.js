const messages = window.tiy.messages[window.tiy.lang.code];
const focusAllowed = window.tiy.focus;

// Append feedback messages to programmeStr.
const feedbacks = programmeStr.map(function (pArr, i) {
  return pArr.concat(messages.feedbacks[i]);
});

let close_flag = '';
let count = 0;
let isPopupOpen;
(function () {
  let lines;
  const leftIndentOffset = 50;
  const attempted = [];

  let codeTag = '';
  let inputVal = '';
  let incorrectFB = '';

  $.fn.NotepadWidget = function () {
    init();
  };

  function init() {
    $('#activity').empty();
    lines = feedbacks.length;

    populateInputs();
  }

  function populateInputs() {
    let inputTag = '';
    let w = 0;

    codeTag = `<div><span style="" id="span_${count}" class="cmdtext" >${String(
      feedbacks[count][0]
    )}</span>`;

    if (String(feedbacks[count][2]) == 'write') {
      if (String(feedbacks[count][4]) == 'password') {
        inputTag = `${codeTag}<input id=${count} class="userInput" type="password" name="value" value="" maxlength=""/></div>`;
      } else {
        inputTag = `${codeTag}<input id=${count} class="userInput" type="text" name="value" value="" maxlength="" /></div>`;
      }
    } else {
      inputTag = `${codeTag}</div>`;
    }

    $(inputTag).appendTo($('#activity'));

    if (String(feedbacks[count][2]) == 'write') {
      w = $('input').width() - $('#span_0').width();
      // alert('w '+ w);
      // $('input').css('width', `${w}px`);
      if (count == 0 && focusAllowed) {
        $('input').eq(0).focus();
      }
      bindInputEvent();
    }

    // It will be last statement of the programme to display on screen.
    if (String(feedbacks[count][2]) == '') {
      /*
			codeTag = '<div><span style="" id="span_'+count+'" class="cmdtext" >' + String(feedbacks[count][0]) + '</span></div>';
			$(codeTag).appendTo($('#activity'));
			*/
      // scrolldown
      $('#activity').animate({ scrollTop: $('#activity').height() }, 500);

      setTimeout(function () {
        showFeedback(true);
      }, 2000);
    }
    /* $('#instruction_text ol li').removeClass('active').eq(count).addClass('active'); */
  }
  function has_correct_fb() {
    return feedbacks[count][5] != undefined && String(feedbacks[count][5]) != '';
  }
  function bindInputEvent() {
    if (!isPopupOpen && focusAllowed) {
      $('input').eq(0).focus();
    }
    // Event to handle Enter key event.
    $('.userInput').keypress(function (event) {
      if (String(feedbacks[count][4]) == 'password') {
        $('input').css('color', '#300924');
      }

      if (event.keyCode == 13) {
        inputVal = $.trim($(this).val());
        correctVal = String(feedbacks[count][1]);
        inputVal = inputVal.toLowerCase();

        let val1 = '';
        if (inputVal == correctVal) {
          val1 = $(`#span_${count}`).html();
          $(`#span_${count}`).html(val1 + inputVal);
          $(this).remove();

          if (has_correct_fb()) {
            show_correct_fb();
          }
          count++;
          populateInputs();
        } else {
          $(this).css({ border: '0px solid red' });
          $('input').val('');
          $('input').css('color', '#fff');
          incorrectFB = String(feedbacks[count][3]);
          showFeedback(false);
        }
      }
    });
  }

  // Show feedback popup
  function showFeedback(flag) {
    isPopupOpen = true;
    $('input').blur();
    let fbStr = '';
    if (flag) {
      // Set correct feedback text and their style;
      fbStr = messages.success[0];
      // Set delay to show popup

      close_flag = true;
      updateFeedbackPosition();
      $('.overlay-bg').show();

      $('input').remove();
    } else {
      // Set incorrect feedback text and their style;
      fbStr = `<span style="font-size:16px; color:#3c3c3c"><span>${incorrectFB}</span>`;
      // Set delay to show popup
      setTimeout(function () {
        // display your popup
        updateFeedbackPosition();
        $('.overlay-bg').show();
      }, 100);
    }

    // Put content in feedback popup.
    $('.overlay-content').html(fbStr);
  }
  function show_correct_fb() {
    isPopupOpen = true;
    $('input').blur();
    const fbStr = `<span style="font-size:16px; color:#3c3c3c"><span>${feedbacks[count][5]}</span>`;
    // Set delay to show popup
    setTimeout(function () {
      // display your popup
      updateFeedbackPosition();
      $('.overlay-bg').show();
    }, 800);
    // Put content in feedback popup.
    $('.overlay-content').html(fbStr);
  }
  function updateFeedbackPosition() {
    const parentW = $('.overlay-bg').width();
    const parentH = $('.overlay-bg').height();

    const childW = $('.overlay-popup').width();
    const childH = $('.overlay-popup').height();

    const topPos = parentH / 2 - childH / 2;
    const leftPos = parentW / 2 - childW / 2;
    $('.overlay-popup').css({ top: topPos - 30 });
    $('.overlay-popup').css({ left: leftPos });
  }
})(jQuery);
