/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
	use \ (backslash) if apostrop or double quotes comes between command.
*/

/*
0 - code string (Code need to be paste as 1st parameter)
1 - Answer string (which will display next line)  if you have else keep blank it.
2 - to show input box need to pass "write" else keep blank it.
*/
const programmeStr = [
  ['student:/tmp> ', 'ping google.com', 'write'],
  [
    'PING google.com (74.125.236.66) 56(84) bytes of data.<br/>64 bytes from maa03s05-in-f2.1e100.net (74.125.236.66): icmp_seq=1 ttl=128 time=79.8 ms<br/>64 bytes from maa03s05-in-f2.1e100.net (74.125.236.66): icmp_seq=2 ttl=128 time=56.5 ms<br/>64 bytes from maa03s05-in-f2.1e100.net (74.125.236.66): icmp_seq=3 ttl=128 time=54.3 ms<br/>64 bytes from maa03s05-in-f2.1e100.net (74.125.236.66): icmp_seq=4 ttl=128 time=48.9 ms<br/>64 bytes from maa03s05-in-f2.1e100.net (74.125.236.66): icmp_seq=5 ttl=128 time=56.4 ms<br/>64 bytes from maa03s05-in-f2.1e100.net (74.125.236.66): icmp_seq=6 ttl=128 time=50.8 ms<br/>64 bytes from maa03s05-in-f2.1e100.net (74.125.236.66): icmp_seq=7 ttl=128 time=47.8 ms<br/>64 bytes from maa03s05-in-f2.1e100.net (74.125.236.66): icmp_seq=8 ttl=128 time=54.2 ms<br/>64 bytes from maa03s05-in-f2.1e100.net (74.125.236.66): icmp_seq=9 ttl=128 time=62.8 ms<br/>64 bytes from maa03s05-in-f2.1e100.net (74.125.236.66): icmp_seq=10 ttl=128 time=56.2 ms<br/>64 bytes from maa03s05-in-f2.1e100.net (74.125.236.66): icmp_seq=11 ttl=128 time=60.2 ms<br/>^C<br/><br/>--- google.com ping statistics ---<br/>11 packets transmitted, 11 received, 0% packet loss, time 10153ms<br/>rtt min/avg/max/mdev = 47.846/57.133/79.855/8.367 ms<br/>student:/tmp> ',
    'route -n',
    'write',
  ],
  [
    'Kernel IP routing table<br/><table><tr><td width="120">Destination</td><td width="120">Gateway</td><td width="120">Genmask</td><td width="70">Flags</td><td width="60">Metric</td><td width="60">Ref</td><td width="60">Use</td><td width="60">Iface</td></tr><tr><td width="120">192.168.127.0</td><td width="120">0.0.0.0</td><td width="120">255.255.255.0</td><td width="70">U</td><td width="60">1</td><td width="60">0</td><td width="60">0</td><td width="60">eth0</td></tr><tr><td width="100">0.0.0.0 </td><td width="100">192.168.127.2</td><td width="100">.0.0.</td><td width="70">UG</td><td width="60">0</td><td width="60">0</td><td width="60">0</td><td width="60">eth0</td></tr></table>student:/tmp> ',
    'traceroute google.com',
    'write',
  ],
  [
    'traceroute to google.com <http://google.com> (74.125.225.34), 30 hops max, 60 byte packets <table width="100%"> <tr> <td>1</td> <td>10.0.0.1 (10.0.0.1)  0.351 ms  0.471 ms 0.507 ms</td> </tr> <tr> <td>2</td> <td>Wireless_Broadband_Router.home (192.168.1.1)  3.348 ms  17.136 ms 17.096 ms</td> </tr> <tr> <td>3</td> <td>static-50-53-96-1.bvtn.or.frontiernet.net <http://static-50-53-96-1.bvtn.or.frontiernet.net> (50.53.96.1)  17.071 ms  17.110 ms  17.103 ms</td> </tr> <tr> <td>4</td> <td>50.38.7.49 (50.38.7.49)  17.113 ms  17.510 ms  20.202 ms</td> </tr> <tr> <td>5</td> <td>ae2---0.cor02.bvtn.or.frontiernet.net <http://ae2---0.cor02.bvtn.or.frontiernet.net> (74.40.1.181)  24.557 ms  24.427 ms  27.187 ms</td> </tr> <tr> <td>6</td> <td>ae0---0.cor01.bvtn.or.frontiernet.net <http://ae0---0.cor01.bvtn.or.frontiernet.net> (74.40.1.185)  29.535 ms  13.757 ms  16.434 ms</td> </tr> <tr> <td>7</td> <td>ae4---0.cor01.sttl.wa.frontiernet.net <http://ae4---0.cor01.sttl.wa.frontiernet.net> (74.40.1.221)  16.596 ms  17.407 ms  17.384 ms</td> </tr> <tr> <td>8</td> <td>ae0---0.cbr01.sttl.wa.frontiernet.net <http://ae0---0.cbr01.sttl.wa.frontiernet.net> (74.40.5.122)  17.347 ms  18.684 ms 18.703 ms</td> </tr> <tr> <td>9</td> <td>74.40.26.177 (74.40.26.177)  21.286 ms 21.381 ms  21.516 ms</td> </tr> <tr> <td>10</td> <td>66.249.94.212 (66.249.94.212)  23.222 ms 26.029 ms 66.249.94.214 (66.249.94.214) 26.047 ms</td> </tr> <tr> <td>11</td> <td>66.249.94.197 <tel:11%C2%A0%2066.249.94.197> (66.249.94.197 <tel:%2866.249.94.197>)  28.859 ms 66.249.94.195 (66.249.94.195)  28.598 ms  16.499 ms</td> </tr> <tr> <td>12</td> <td>72.14.239.208 (72.14.239.208)  55.576 ms 58.751 ms  57.282 ms</td> </tr> <tr> <td>13</td> <td>209.85.254.239 (209.85.254.239)  57.260 ms  59.013 ms  59.045 ms</td> </tr> <tr> <td>14</td> <td>209.85.250.28 (209.85.250.28)  61.836 ms 61.846 ms  63.991 ms</td> </tr> <tr> <td>15</td> <td>ord08s06-in-f2.1e100.net <http://ord08s06-in-f2.1e100.net> (74.125.225.34)  69.225 ms  69.433 ms  71.136 ms</td> </tr></table>student:/tmp> ',
    '',
    '',
  ],
];
