/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
*/

/*
0 - code string (Code need to be paste as 1st parameter)
1 - Answer string (which will display next line)  if you have else keep blank it.
2 - to show input box need to pass "write" else keep blank it.
*/
const programmeStr = [
  ['student:/tmp> ', 'wget http://www.linuxfoundation.org/about/faq', 'write'],
  [
    ' --2017-01-19 11:45:52--  http://www.linuxfoundation.org/about/faq<BR/>Resolving www.linuxfoundation.org (www.linuxfoundation.org)... 140.211.169.4<BR/>Connecting to www.linuxfoundation.org (www.linuxfoundation.org)|140.211.169.4|:80... connected.<BR/>HTTP request sent, awaiting response... 301 Moved Permanently<BR/>Location: https://www.linuxfoundation.org/about/faq [following]<BR/>--2017-01-19 11:45:53--  https://www.linuxfoundation.org/about/faq<BR/>Connecting to www.linuxfoundation.org (www.linuxfoundation.org)|140.211.169.4|:443... connected.<BR/>HTTP request sent, awaiting response... 301 Moved Permanently<BR/>Location: https://www.linuxfoundation.org/about [following]<BR/>--2017-01-19 11:45:53--  https://www.linuxfoundation.org/about<BR/>Reusing existing connection to www.linuxfoundation.org:443.<BR/>HTTP request sent, awaiting response... 200 OK<BR/>Length: unspecified [text/html]<BR/>Saving to: ‘faq ’<br/>    [ <=>                                                       ] 50,857      --.-K/s   in 0.1s<br><br/>2017-01-19 11:45:53 (501 KB/s) - ‘faq ’ saved [50857]<br/>student:/tmp> ',
    'curl -o lwn.out https://lwn.net',
    'write',
  ],
  [
    '% Total    % Received % Xferd  Average Speed   Time    Time     Time  Current<br>                                 Dload  Upload   Total   Spent    Left  Speed</br>100 31346  100 31346    0     0  72957      0 --:--:-- --:--:-- --:--:-- 73067</br>student:/tmp> ',
    '',
    '',
  ],
];
