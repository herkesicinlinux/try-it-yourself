/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
*/

/*
0 - code string (Code need to be paste as 1st parameter)
1 - Answer string (which will display next line)  if you have else keep blank it.
2 - to show input box need to pass "write" else keep blank it.
*/
const programmeStr = [
  ['student:/tmp> ', 'ftp ftp.gnu.org', 'write'],
  [
    'Connected to ftp.gnu.org.<br/>220 GNU FTP server ready.<br/>Name (ftp.gnu.org:test1):',
    'anonymous',
    'write',
  ],
  [
    '230-Due to U.S. Export Regulations, all cryptographic software on this<br/>230-site is subject to the following legal notice:<br/>230-<br/>230-    This site includes publicly available encryption source code<br/>230-    which, together with object code resulting from the compiling of<br/>230-    publicly available source code, may be exported from the United<br/>230-    States under License Exception "TSU" pursuant to 15 C.F.R. Section<br/>230-    740.13(e).<br/>230-<br/>230-This legal notice applies to cryptographic software only. Please see<br/>230-the Bureau of Industry and Security (www.bxa.doc.gov) for more<br/>230-information about current U.S. regulations.<br/>230 Login successful.<br/>Remote system type is UNIX.<br/>Using binary mode to transfer files.<br/>ftp>',
    'get welcome.msg',
    'write',
  ],
  [
    'local: welcome.msg remote: welcome.msg<br/>200 PORT command successful. Consider using PASV.<br/>150 Opening BINARY mode data connection for welcome.msg (954 bytes).<br/>ftp>',
    '',
    '',
  ],
];
