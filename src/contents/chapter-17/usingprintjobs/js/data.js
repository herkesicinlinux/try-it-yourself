/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
	use \ (backslash) if apostrop or double quotes comes between command.
*/

/*
0 - code string (Code need to be paste as 1st parameter)
1 - Answer string (which will display next line)  if you have else keep blank it.
2 - to show input box need to pass "write" else keep blank it.
*/
const programmeStr = [
  ['[test3@CentOS ~]$ ', 'cancel 11', 'write'],
  ['[test3@CentOS ~]$ ', 'lpstat -p -d', 'write'],
  [
    'printer HP-LaserJet now printing HP-LaserJet-8. enabled since Mon 09 Jun 2014 01:18:09 AM IST<br/><span style="margin_small">Starting GPL Ghostscript 8.70...</span><br/>system default destination: HP-LaserJet<br/>printer Epson-new now printing Epson-new 1. enabled since Mon 09 Jun 2014 02:29:09 AM IST<br/><span style="margin_small">Starting GPL Ghostscript 8.70...</span><br/>system default destination: Epson-new<br/>[test3@CentOS ~]$ ',
    'lpmove 8 epson-new',
    'write',
  ],
  ['[test3@CentOS ~]$ ', 'lpstat -a', 'write'],
  [
    'HP-LaserJet accepting requests since Mon 09 Jun 2014 01:18:09 AM IST<br/>Epson-new accepting requests since Mon 09 Jun 2014 02:29:09 AM IST<br/>[test3@CentOS ~]$ ',
    '',
    '',
  ],
];
