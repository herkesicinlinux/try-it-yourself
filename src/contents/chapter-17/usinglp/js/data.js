/* Important notes
	\ will not work, so need to put double \\ for each single \
	for < use &lt; and > use &gt;
	use \ (backslash) if apostrop or double quotes comes between command.
*/

/*
0 - code string (Code need to be paste as 1st parameter)
1 - Answer string (which will display next line)  if you have else keep blank it.
2 - to show input box need to pass "write" else keep blank it.
*/
const programmeStr = [
  ['student:/tmp> ', 'lp testfile.pdf', 'write'],
  [
    'request id is Brother HLL2380DW (1 file(s))<br/>student:/tmp> ',
    'lp -d hp-laserjet testfile.pdf',
    'write',
  ],
  ['request id is HP-LaserJet-6 (1 file(s))<br/>student:/tmp> ', 'lp -n 2 testfile.pdf', 'write'],
  [
    'request id is Brother HLL2380DW (1 file(s))<br/>student:/tmp> ',
    'lpoptions -d hp-laserjet',
    'write',
  ],
  [
    "auth-info-required=none copies=1 device-uri=socket://10<br/>.0.2.15:9100 finishings=3 job-hold-until=no-hold job-pr<br/>Ioroty=50 job-sheets=none,none marker-change-teim=0 med<br/>ia=na_letter_8.5x11in number-up=1 printer-commands=Auto<br/>Configure,Clean,PrintSelfTestPage printer-info='HP LaserJ<br/>et Series PCL 6 CUPS' printer-state=3 printer-state-cha<br/>nge-time=1402256636 printer-state-reasons=none printer-<br/>type=8532052 printer-uri-supported=ipp://localhost:631/<br/>printer/HP-LaserJet sides=one-sided<br/>student:/tmp> ",
    'lpq -a',
    'write',
  ],
  [
    '<table width="100%"><tr><td>Rank</td> <td>Owner</td> <td>Job</td> <td>File(s)</td> <td>Total Size</td> </tr> <tr> <td>active</td> <td>student</td> <td>8</td> <td>printcap</td> <td>1024 bytes</td> </tr> <tr> <td>1st</td> <td>student</td> <td>9</td> <td>printcap</td> <td>1024 bytes</td> </tr> </table>student:/tmp> ',
    '',
    '',
  ],
];
