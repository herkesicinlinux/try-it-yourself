window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Using <code>tr</code>',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'View the <b>cities</b> file using <code>cat</code>.',
          'Convert lowercase characters to uppercase using <code>tr</code>.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Please type "cat cities" and press the <b>Enter</b> key.',
        'You have successfully completed the assigned task. Now you will convert the lowercase characters to uppercase using tr.',
      ],
      ['Please type "cat cities | tr a-z A-Z" and press the <b>Enter</b> key.', ''],
      ['', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: <code>tr</code> Kullanımı',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          '<code>cat</code> kullanarak <b>cities</b> dosyasını görüntüleyin.',
          '<code>tr</code> kullanarak küçük harfli karakterleri büyük harfe dönüştürün.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Lütfen "cat cities" yazın ve <b>Enter</b> tuşuna basın.',
        'Görevi başarıyla tamamladınız. Şimdi, tr kullanarak küçük harfli karakterleri büyük harfe çevireceksiniz.',
      ],
      ['Lütfen "cat cities | tr a-z A-Z" yazın ve <b>Enter</b> tuşuna basın.', ''],
      ['', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
