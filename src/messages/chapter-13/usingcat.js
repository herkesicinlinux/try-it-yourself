window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Using <code>cat</code>',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Using <code>cat</code>, create a file named <b>test1</b>, type "Line1" and save it by pressing Ctrl-D.',
          'Using <code>cat</code>, create a file named <b>test2</b>, type "Line2" and save it by pressing Ctrl-D.',
          'Using <code>cat</code>, create a file named <b>test3</b>, type "Line3" and save it by pressing Ctrl-D.',
          'Concatenate the files <b>test1</b> and <b>test2</b> into a file named <b>newtest</b>.',
          'View the contents of <b>newtest</b>, using <code>cat</code>.',
          'Append the file <b>test3</b> at the end <b>newtest</b>.',
          'View the contents of the <b>newtest</b> file, using <code>cat</code>.',
          'Append the following text to <b>newtest</b>: "Line4".',
          'View the contents of the <b>newtest</b> file, using <code>cat</code>.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Please type "cat > test1" and press <b>Enter</b>.',
        '',
        'You have successfully completed the assigned task. Now you will type the text in test1.',
      ],
      [
        'Please type "Line1" and press <b>Enter</b> followed by "<b>CTRL-D</b>" to save the file.',
        'paragraph',
        'You have successfully completed the assigned task. Now you will create the file test2.',
      ],
      [
        'Please type "cat > test2" and press <b>Enter</b>.',
        '',
        'You have successfully completed the assigned task. Now you will type the text in test2.',
      ],
      [
        'Please type "Line2" and press <b>Enter</b> followed by "<b>CTRL-D</b>" to save the file.',
        'paragraph',
        'You have successfully completed the assigned task. Now you will create the file test3.',
      ],
      [
        'Please type "cat > test3" and press <b>Enter</b>.',
        '',
        'You have successfully completed the assigned task. Now you will type the text in test3.',
      ],
      [
        'Please type "Line3" and press<b>Enter</b> followed by "<b>CTRL-D</b>" to save the file.',
        'paragraph',
        'You have successfully completed the assigned task. Now you will combine test1 and test2 using cat.',
      ],
      [
        'Please type "cat test1 test2 > newtest" and press the <b>Enter</b> key.',
        '',
        'You have successfully completed the assigned task. Now you will view newtest using cat.',
      ],
      [
        'Please type "cat newtest" and press the <b>Enter</b> key.',
        '',
        'You have successfully completed the assigned task. Now you will append test3 at the end of newtest.',
      ],
      [
        'Please type "cat test3 >> newtest" and press the <b>Enter</b> key.',
        '',
        'You have successfully completed the assigned task. Now you will view the newtest file using cat.',
      ],
      [
        'Please type "cat newtest" and press the <b>Enter</b> key.',
        '',
        'You have successfully completed the assigned task. Now you will append specific text at the end of the newtest file.',
      ],
      [
        'Please type "cat >> newtest" and press the <b>Enter</b> key.',
        '',
        'You have successfully completed the assigned task. Now you will type the text in the newtest file.',
      ],
      [
        'Please type "Line4" and press <b>Enter</b> followed by <b>CTRL-D</b> to save the file.',
        'paragraph',
        'You have successfully completed the assigned task. Now you will view newtest using cat.',
      ],
      ['Please type type "cat newtest" and press <b>Enter</b>.', '', ''],
      ['', '', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: <code>cat</code> Kullanımı',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          '<code>cat</code> kullanarak, <b>test1</b> adlı bir dosya oluşturun, "Line1" yazın ve Ctrl-D tuşlarına basarak kaydedin.',
          '<code>cat</code> kullanarak, <b>test2</b> adlı bir dosya oluşturun, "Line2" yazın ve Ctrl-D tuşlarına basarak kaydedin.',
          '<code>cat</code> kullanarak, <b>test3</b> adlı bir dosya oluşturun, "Line3" yazın ve Ctrl-D tuşlarına basarak kaydedin.',
          '<b>test1</b> ve <b>test2</b> dosyalarını <b>newtest</b> adlı bir dosyada birleştirin.',
          "<code>cat</code> kullanarak, <b>newtest</b>'in içeriğini görüntüleyin.",
          "<b>newtest</b>'in sonuna <b>test3</b> dosyasını ekleyin.",
          '<code>cat</code> kullanarak, <b>newtest</b> dosyasının içeriğini görüntüleyin.',
          'Şu metni <b>newtest</b>\'e ekleyin: "Line4".',
          '<code>cat</code> kullanarak, <b>newtest</b> dosyanın içeriğini görüntüleyin.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Lütfen "cat > test1" yazın ve <b>Enter</b> tuşuna basın.',
        '',
        "Görevi başarıyla tamamladınız. Şimdi test1'e metin gireceksiniz.",
      ],
      [
        'Lütfen "Line1" yazın ve dosyayı kaydetmek için <b>Enter</b> tuşuna ve ardından "<b>CTRL-D</b>" tuşlarına basın.',
        'paragraph',
        'Görevi başarıyla tamamladınız. Şimdi test2 dosyasını oluşturacaksınız.',
      ],
      [
        'Lütfen "cat > test2" yazın ve <b>Enter</b> tuşuna basın.',
        '',
        "Görevi başarıyla tamamladınız. Şimdi test2'ye metin gireceksiniz.",
      ],
      [
        'Lütfen "Line2" yazın ve dosyayı kaydetmek için <b>Enter</b> tuşuna ve ardından "<b>CTRL-D</b>" tuşlarına basın.',
        'paragraph',
        'Görevi başarıyla tamamladınız. Şimdi test3 dosyasını oluşturacaksınız.',
      ],
      [
        'Lütfen "cat > test3" yazın ve <b>Enter</b> tuşuna basın.',
        '',
        "Görevi başarıyla tamamladınız. Şimdi test3'e metin gireceksiniz.",
      ],
      [
        'Lütfen "Line3" yazın ve dosyayı kaydetmek için <b>Enter</b> tuşuna ve ardından "<b>CTRL-D</b>" tuşlarına basın.',
        'paragraph',
        "Görevi başarıyla tamamladınız. Şimdi cat kullanarak test1 ve test2'yi birleştireceksiniz.",
      ],
      [
        'Lütfen "cat test1 test2 > newtest" yazın ve <b>Enter</b> tuşuna basın.',
        '',
        "Görevi başarıyla tamamladınız. Şimdi cat kullanarak newtest'in içeriğini görüntüleyeceksiniz.",
      ],
      [
        'Lütfen "cat newtest" yazın ve <b>Enter</b> tuşuna basın.',
        '',
        "Atanan görevi başarıyla tamamladınız. Şimdi newtest'in sonuna test3'ü ekleyeceksiniz.",
      ],
      [
        'Lütfen "cat test3 >> newtest" yazın ve <b>Enter</b> tuşuna basın.',
        '',
        'Görevi başarıyla tamamladınız. Şimdi newtest dosyasını cat kullanarak görüntüleyeceksiniz.',
      ],
      [
        'Lütfen "cat newtest" yazın ve <b>Enter</ b> tuşuna basın.',
        '',
        'Görevi başarıyla tamamladınız. Şimdi newtest dosyasının sonuna belirli bir metin ekleyeceksiniz.',
      ],
      [
        'Lütfen "cat >> newtest" yazın ve <b>Enter</b> tuşuna basın.',
        '',
        'Görevi başarıyla tamamladınız. Şimdi metni newtest dosyasına yazacaksınız.',
      ],
      [
        'Lütfen "Line4" yazın ve dosyayı kaydetmek için <b>Enter</b> tuşuna ve ardından <b>CTRL-D</b> tuşlarına basın.',
        'paragraph',
        "Görevi başarıyla tamamladınız. Şimdi cat kullanarak newtest'in içeriğini görüntüleyeceksiniz. ",
      ],
      ['Lütfen "cat newtest" yazın ve <b>Enter</b> tuşuna basın.', '', ''],
      ['', '', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
