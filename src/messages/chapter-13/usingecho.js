window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Using <code>echo</code>',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Using <code>echo</code>, display a string: "This is a test.".<br/>[Note: Do not change the case and add additional characters like quotes, period and comma.]',
          'View the value of environmental variable <code>$SHELL</code>.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Please type "echo This is a test." and press the <b>Enter</b> key.',
        'You have successfully completed the assigned task. Now you will display the value of environment variable $SHELL using the echo command.',
      ],
      ['Please type "echo $SHELL" and press the <b>Enter</b> key.', ''],
      ['', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: <code>echo</code> Kullanımı',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          '<code>echo</code> kullanarak şu dizgeyi görüntüleyin: "This is a test.".<br/>[Not: Büyük/küçük harf değiştirmeyin ve tırnak, nokta ve virgül gibi ek karakterler eklemeyin.]',
          '<code>$SHELL</code> ortam değişkeninin değerini görüntüleyin.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Lütfen "echo This is a test." yazın ve <b>Enter</b> tuşuna basın.',
        'Görevi başarıyla tamamladınız. Şimdi, echo komutunu kullanarak $SHELL ortam değişkeninin değerini görüntüleyeceksiniz.',
      ],
      ['Lütfen "echo $SHELL" yazın ve <b>Enter</b> tuşuna basın.', ''],
      ['', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
