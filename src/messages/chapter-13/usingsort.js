window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Using <code>sort</code> and <code>uniq</code>',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'View the <b>planets</b> file using <code>cat</code>.',
          'Sort the <b>planets</b> file using <code>sort</code>.',
          'Remove duplicate entries from the <b>planets</b> file using <code>uniq</code>.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Please type "cat planets" and press the <b>Enter</b> key.',
        'You have successfully completed the assigned task.',
      ],
      [
        'Please type "sort planets" and press the <b>Enter</b> key.',
        'You have successfully completed the assigned task.',
      ],
      ['Please type "sort planets | uniq" and press the <b>Enter</b> key.', ''],
      ['', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: <code>sort</code> ve <code>uniq</code> Kullanımı',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          '<code>cat</code> kullanarak <b>planets</b> dosyasını görüntüleyin.',
          '<b>planets</b> dosyasını <code>sort</code> kullanarak sıralayın.',
          '<code>uniq</code> kullanarak <b>planets</b> dosyasından yinelenen girişleri kaldırın.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Lütfen "cat planets" yazın ve <b>Enter</b> tuşuna basın.',
        'Görevi başarıyla tamamladınız.',
      ],
      [
        'Lütfen "sort planets" yazın ve <b>Enter</b> tuşuna basın.',
        'Görevi başarıyla tamamladınız.',
      ],
      ['Lütfen "sort planets | uniq" yazın ve <b>Enter</b> tuşuna basın.', ''],
      ['', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
