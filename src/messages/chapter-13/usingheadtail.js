window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Using <code>head</code> and <code>tail</code>',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'View the first five lines of a file named <code><b>newtest</b></code> using <code>head</code>.',
          'View the last five lines of a file named <code><b>newtest</b></code> using <code>tail</code>.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Please type "head -n 5 newtest" or "head -5 newtest" and press the <b>Enter</b> key.',
        'You have successfully completed the assigned task. Now you will view the last five lines of the newtest file using tail.',
      ],
      ['Please type "tail -n 5 newtest" or "tail -5 newtest" and press the <b>Enter</b> key.', ''],
      ['', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: <code>head</code> ve <code>tail</code> Kullanımı',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          '<code>head</code> kullanarak, <code><b>newtest</b></code> adlı dosyanın ilk beş satırını görüntüleyin.',
          '<code>tail</code> kullanarak, <code><b>newtest</b></code> adlı dosyanın son beş satırını görüntüleyin.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Lütfen "head -n 5 newtest" veya "head -5 newtest" yazın ve <b>Enter</b> tuşuna basın.',
        'Görevi başarıyla tamamladınız. Şimdi newtest dosyasının son beş satırını tail kullanarak görüntüleyeceksiniz.',
      ],
      ['Lütfen "tail -n 5 newtest" veya "tail -5 newtest" yazın ve <b>Enter</b> tuşuna basın.', ''],
      ['', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
