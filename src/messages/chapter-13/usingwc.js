window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Using <code>wc</code>',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Print out the number of lines, words, and characters in <b>testfile</b> using <code>wc</code>.',
          'Print out just the number of lines in <b>testfile</b> file using <code>wc</code>.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Please type "wc testfile" and press the <b>Enter</b> key.',
        'You have successfully completed the assigned task.',
      ],
      ['Please type "wc -l testfile" and press the <b>Enter</b> key.', ''],
      ['', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: <code>wc</code> Kullanımı',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          '<code>wc</code> kullanarak, <b>testfile</b> dosyasındaki satır, kelime ve karakter sayısını yazdırın.',
          '<code>wc</code> kullanarak, <b>testfile</b> dosyasındaki yalnızca satır sayısını yazdırın.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Lütfen "wc testfile" yazın ve <b>Enter</b> tuşuna basın.',
        'Görevi başarıyla tamamladınız.',
      ],
      ['Lütfen "wc -l testfile" yazın ve <b>Enter</b> tuşuna basın.', ''],
      ['', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
