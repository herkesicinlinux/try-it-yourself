window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Applying System and Display Settings in Ubuntu',
      subtitle: 'Task to be performed:',
      description: '',
      list: {
        type: 'ul',
        items: ['Change the screen resolution to 1024x768 in Ubuntu.'],
      },
      extra: 'Click the System Settings icon in the left panel.',
    },
    feedbacks: [
      ['Please click the <b>System Settings</b> icon.'],
      ['Please click the <b>Displays</b> icon.'],
      [
        'From the <b>Resolution</b> drop-down list, please select <b>1024x768</b>, and click <b>Apply</b> button.',
      ],
      ['Please select <b>1024x768</b>, and click <b>Apply</b> button.'],
      ['Please click <b>Apply</b> button.'],
    ],
    success: ['You have successfully completed the task.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: "Kendin Dene: Ubuntu'da Sistem ve Görüntü Ayarlarını Uygulama",
      subtitle: 'Gerçekleştirilecek görev:',
      description: '',
      list: {
        type: 'ul',
        items: ["Ubuntu'da ekran çözünürlüğünü 1024x768 olarak değiştirin."],
      },
      extra: 'Sol paneldeki Sistem Ayarları simgesine tıklayın.',
    },
    feedbacks: [
      ['Lütfen <b>Sistem Ayarları</b> simgesini tıklayın.'],
      ['Lütfen <b>Displays</b> simgesini tıklayın.'],
      [
        "<b>Resolution</b> açılır listesinden, lütfen <b>1024x768</b>'i seçin ve <b>Apply</b> düğmesini tıklayın. ",
      ],
      ["Lütfen <b>1024x768</b>'i seçin ve <b>Apply</b> düğmesini tıklayın. "],
      ['Lütfen <b>Apply</b> düğmesini tıklayın.'],
    ],
    success: ['Görevi başarıyla tamamladınız.'],
  },
};
