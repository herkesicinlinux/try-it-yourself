window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: '',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: ['', ''],
      },
      extra: '',
    },
    feedbacks: [[''], [''], ['']],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: '',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: ['', ''],
      },
      extra: '',
    },
    feedbacks: [[''], [''], ['']],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
