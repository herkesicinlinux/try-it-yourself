window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title:
        'Try-It-Yourself: Viewing the Filesystem Hierarchy from the Graphical Interface in Ubuntu',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Explore <b>/etc/avahi</b> directory.',
          'Check the current path of <b>avahi</b> directory.',
        ],
      },
    },
    feedbacks: [
      ['Click the file manager icon on the side panel of the desktop.'],
      ['Click <b>Computer</b> in the <b>Places</b> pane inside this window.'],
      ['Double-click the <b>etc</b> directory to open it.'],
      ['Double-click the <b>avahi</b> directory.'],
      [
        'Press <b>Ctrl - L</b> to see the current location. You can view the path in the address bar of this window.',
      ],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: "Kendin Dene: Ubuntu Grafik Arayüzü'nden Dosya Sistemi Hiyerarşisini Görüntüleme",
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          '<b>/etc/avahi</b> dizinini keşfedin.',
          '<b>avahi</b> dizininin mevcut yolunu kontrol edin.',
        ],
      },
    },
    feedbacks: [
      ['Masaüstünün yan panelindeki dosya yöneticisi simgesine tıklayın.'],
      ['<b>Places</b> panelindeki <b>Computer</b> seçeneğine tıklayın.'],
      ['Açmak için <b>etc</b> dizinine çift tıklayın.'],
      ['Açmak için <b>avahi</b> dizinine çift tıklayın.'],
      [
        'Mevcut konumu görmek için <b>Ctrl - L</b> tuşlarına basın. Yolu, bu pencerenin adres çubuğunda görüntüleyebilirsiniz.',
      ],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
