window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Locating Files',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Find all files with the extension <code><b>.doc</b></code>.',
          'Copy the file named <code><b>pdb.doc</b></code> from <code>/usr/lib/python2.7</code> to the current working directory as <code><b>Myfile.doc</b></code>.',
          'Update the database used by <b>locate</b> by running <b>updatedb</b>.',
          'Locate the file <code><b>Myfile.doc</b></code>. (Remember that filenames are case sensitive!).',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'At the command prompt, please type "locate .doc" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Now you will copy pdb.doc from /usr/lib/python2.7 to your current working directory.',
      ],
      [
        'At the command prompt, please type "cp /usr/lib/python2.7/pdb.doc Myfile.doc" and press <b>Enter</b>.',
        'You have successfully copied the pdb.doc from  /usr/lib/python2.7 to your current working directory. Now you will update the database.',
      ],
      [
        'At the command prompt, please type "sudo updatedb" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Now you will locate the Myfile.doc file.',
      ],
      ['At the command prompt, please type "locate Myfile.doc" and press <b>Enter</b>.', ''],
      ['', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: Dosyaları Bulma',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          '<code><b>.doc</b></code> uzantılı tüm dosyaları bulun.',
          '<code><b>pdb.doc</b></code> adlı dosyayı, <code>/usr/lib/python2.7</code> dizininden mevcut çalışma dizinine, <code><b>Myfile.doc</b></code> adıyla kopyalayın.',
          '<b>updatedb</b> kullanarak <b>locate</b> tarafından kullanılan veritabanını güncelleyin.',
          '<code><b>Myfile.doc</b></code> dosyasını bulun. (Dosya adlarının büyük/küçük harfe duyarlı olduğunu unutmayın!).',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Komut isteminde, lütfen "locate .doc" yazın ve <b>Enter</b>\'a basın.',
        "Görevi başarıyla tamamladınız. Şimdi /usr/lib/python2.7'den pdb.doc dosyasını mevcut çalışma dizininize kopyalayacaksınız.",
      ],
      [
        'Komut istemine, lütfen "cp /usr/lib/python2.7/pdb.doc Myfile.doc" yazın ve <b>Enter</b>\'a basın.',
        "pdb.doc dosyasını /usr/lib/python2.7'den mevcut çalışma dizininize başarıyla kopyaladınız. Şimdi veritabanını güncelleyeceksiniz.",
      ],
      [
        'Komut isteminde, lütfen "sudo updatedb" yazın ve <b>Enter</b>\'a basın.',
        'Görevi başarıyla tamamladınız. Şimdi Myfile.doc dosyasını bulacaksınız.',
      ],
      ['Komut isteminde, lütfen "locate Myfile.doc" yazın ve <b>Enter</b> tuşuna basın.', ''],
      ['', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
