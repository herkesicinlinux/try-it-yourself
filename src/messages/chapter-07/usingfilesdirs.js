window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Working with Files and Directories at the Command Prompt',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Using <b>touch</b>, create <code><b>file1</b></code> and <code><b>file2</b></code> (two empty files) with timestamp: 14 March 2018 2:00 PM.',
          'Check for the existence of <code><b>file1</b></code> and <code><b>file2</b></code> using <b>ls -l</b>.',
          'Rename <code><b>file1</b></code> to <code><b>new_file1</b></code> using <b>mv</b>.',
          'Remove <code><b>file2</b></code> using <b>rm</b> without any options.',
          'Remove <code><b>new_file1</b></code> using <b>rm</b> without any options.',
          'Create a directory named <code><b>dir1</b></code>, using <b>mkdir</b>.',
          'Remove <code><b>dir1</b></code> using <b>rmdir</b> without any options.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'At the command prompt, please type "touch -t 1803141400 file1 file2" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Now you will check for the existence of file1 and file2 files using ls -l command.',
      ],
      [
        'At the command prompt, please type "ls -l file1 file2" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Now you will rename the file1 file to new_file1.',
      ],
      [
        'At the command prompt, please type "mv file1 new_file1" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Now you will remove the file2 file.',
      ],
      [
        'At the command prompt, please type "rm file2" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Now you will remove the new_file1 file.',
      ],
      [
        'At the command prompt, please type "rm new_file1" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Now you will create the dir1 directory.',
      ],
      [
        'At the command prompt, please type "mkdir dir1" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Now you will remove the dir1 directory.',
      ],
      ['At the command prompt, please type "rmdir dir1" and press <b>Enter</b>.', ''],
      ['', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: Komut İsteminde Dosyalar ve Dizinler ile Çalışma',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          '<b>touch</b> kullanarak, 14 Mart 2018 14:00 zaman damgası ile, <code><b>file1</b></code> ve <code><b>file2</b></code> adında iki boş dosya oluşturun.',
          "<b>ls -l</b> kullanarak <code><b>file1</b></code> ve <code><b>file2</b></code>'nin varlığını kontrol edin.",
          '<code><b>file1</b></code> dosyasını <b>mv</b> kullanarak <code><b>new_file1</b></code> olarak yeniden adlandırın.',
          '<code><b>file2</b></code> dosyasını, herhangi bir seçenek olmadan <b>rm</b> kullanarak kaldırın.',
          '<code><b>new_file1</b></code> dosyasını, herhangi bir seçenek olmadan <b>rm</b> kullanarak kaldırın.',
          '<b>mkdir</b> kullanarak <code><b>dir1</b></code> adlı bir dizin oluşturun.',
          "<code><b>dir1</b></code>'i herhangi bir seçenek olmadan <b>rmdir</b> kullanarak kaldırın.",
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Komut isteminde, lütfen "touch -t 1803141400 file1 file2" yazın ve <b>Enter</b>\'a basın.',
        'Görevi başarıyla tamamladınız. Şimdi ls -l komutunu kullanarak file1 ve file2 dosyalarının varlığını kontrol edeceksiniz.',
      ],
      [
        'Komut isteminde, lütfen "ls -l file1 file2" yazın ve <b>Enter</b> tuşuna basın.',
        'Görevi başarıyla tamamladınız. Şimdi file1 dosyasını new_file1 olarak yeniden adlandıracaksınız.',
      ],
      [
        'Komut isteminde, lütfen "mv file1 new_file1" yazın ve <b>Enter</b>\'a basın. ',
        'Görevi başarıyla tamamladınız. Şimdi file2 dosyasını kaldıracaksınız.',
      ],
      [
        'Komut isteminde, lütfen "rm file2" yazın ve <b>Enter</b> tuşuna basın.',
        'Görevi başarıyla tamamladınız. Şimdi new_file1 dosyasını kaldıracaksınız.',
      ],
      [
        'Komut isteminde, lütfen "rm new_file1" yazın ve <b>Enter</b>\'a basın.',
        'Görevi başarıyla tamamladınız. Şimdi dir1 dizinini oluşturacaksınız.',
      ],
      [
        'Komut isteminde, lütfen "mkdir dir1" yazın ve <b>Enter</b>\'a basın.',
        'Görevi başarıyla tamamladınız. Şimdi dir1 dizinini kaldıracaksınız. ',
      ],
      ['Komut isteminde, lütfen "rmdir dir1" yazın ve <b>Enter</b>\'a basın.', ''],
      ['', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
