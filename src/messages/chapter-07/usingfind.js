window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Finding Files in a Directory',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Search for a file with name gcc in the <code>/usr</code> folder using <code>find</code> command.',
          'Search for a directory with filename gcc, in the <code>/usr</code> folder using <code>find</code> command.',
          'Search for files in the current directory which were modified today.',
          'Search for files with size 0 bytes.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'At the command prompt, please type "find /usr -name gcc" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Now you will search for a directory with filename gcc, in the /usr directory.',
      ],
      [
        'At the command prompt, please type "find /usr -type d -name gcc" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Now you will search for files in the current directory which were modified today.',
      ],
      [
        'At the command prompt, please type "find -type f -mtime 0" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Now you will search for files with size 0 bytes.',
      ],
      ['At the command prompt, please type "find -type f -size 0" and press <b>Enter</b>.', ''],
      ['', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: Bir Dizindeki Dosyaları Bulma',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          '<code>/usr</code> dizininde, <code>find</code> komutunu kullanarak gcc adıyla bir dosya arayın.',
          '<code>/usr</code> dizininde, <code>find</code> komutunu kullanarak gcc adıyla bir dizin arayın.',
          'Geçerli dizinde bugün değiştirilen dosyaları arayın.',
          '0 bayt boyutundaki dosyaları arayın.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Komut isteminde, lütfen "find /usr -name gcc" yazın ve <b>Enter</b>\'a basın.',
        'Görevi başarıyla tamamladınız. Şimdi /usr dizininde gcc adına sahip bir dizin arayacaksınız.',
      ],
      [
        'Komut isteminde, lütfen "find /usr -type d -name gcc" yazın ve <b>Enter</b>\'a basın.',
        'Görevi başarıyla tamamladınız. Şimdi mevcut dizinde bugün değiştirilen dosyaları arayacaksınız.',
      ],
      [
        'Komut isteminde, lütfen "find -type f -mtime 0" yazın ve <b>Enter</b> tuşuna basın.',
        'Görevi başarıyla tamamladınız. Şimdi boyutu 0 bayt olan dosyaları arayacaksınız. ',
      ],
      ['Komut isteminde, lütfen "find -type f -size 0" yazın ve <b>Enter</b>\'a basın.', ''],
      ['', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
