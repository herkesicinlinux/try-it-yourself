window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Using bash Wildcards with <code>ls</code>',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'View all the files in the current directory using <code>ls</code> command with <code>-a</code> option.',
          'List (using ls) files with names starting with g and containing five letters.',
          'List (using ls) files whose names begin with mk and end with any characters.',
          'List (using ls) files having five letter names starting with g and second character between a-n.',
          'List (using ls) five letter named files starting with g and not having the second character between a-m.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'At the command prompt, please type "ls -a" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Now you will search for files whose names are of five letters and start with g.',
      ],
      [
        'At the command prompt, please type "ls g????" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Now you will search for files whose names begin with mk and end with any character.',
      ],
      [
        'At the command prompt, please type "ls mk*" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Now you will search for files having five letter names starting with g and second character between a-n.',
      ],
      [
        'At the command prompt, please type "ls g[a-n]???" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Now you will search for five letter named files starting with g and not having the second character between a-m.',
      ],
      ['At the command prompt, please type "ls g[!a-m]???" and press <b>Enter</b>.', ''],
      ['', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: <code>ls</code> ile Bash Joker Karakterlerini (Wildcard) Kullanma',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          '<code>ls</code> komutunu <code>-a</code> seçeneğiyle kullanarak mevcut dizindeki tüm dosyaları görüntüleyin.',
          '<code>ls</code> kullanarak, g ile başlayan ve beş harf içeren adlara sahip dosyaları listeleyin.',
          '<code>ls</code> kullanarak, isimleri mk ile başlayan ve herhangi bir karakterle biten dosyaları listeleyin.',
          '<code>ls</code> kullanarak, g ile başlayan, beş harfli ve ikinci karakteri <b>a-n</b> arasında <b>olan</b> dosyaları listeleyin.',
          '<code>ls</code> kullanarak, g ile başlayan, beş harfli ve ikinci karakteri <b>a-m</b> arasında <b>olmayan</b>  dosyaları listeleyin.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Komut isteminde, lütfen "ls -a" yazın ve <b>Enter</b> tuşuna basın.',
        'You have successfully completed the assigned task. Now you will search for files whose names are of five letters and start with g.',
        'Görevi başarıyla tamamladınız. Şimdi, adı g ile başlayan beş harfli dosyaları arayacaksınız.',
      ],
      [
        'Komut isteminde, lütfen "ls g????" yazın ve <b>Enter</b>\'a basın.',
        'Görevi başarıyla tamamladınız. Şimdi, isimleri mk ile başlayan ve herhangi bir karakterle biten dosyaları arayacaksınız.',
      ],
      [
        'Komut isteminde, lütfen "ls mk*" yazın ve <b>Enter</b> tuşuna basın.',
        'Görevi başarıyla tamamladınız. Şimdi, g ile başlayan ve a-n arasında ikinci karaktere sahip beş harfli dosyaları arayacaksınız.',
      ],
      [
        'Komut isteminde, lütfen "ls g[a-n]???" yazın ve <b>Enter</b>\'a basın',
        'Görevi başarıyla tamamladınız. Şimdi, g ile başlayan ve a-m arasında ikinci karakteri olmayan beş harfli dosyaları arayacaksınız.',
      ],
      ['Komut isteminde, lütfen "ls g[!a-m]???" yazın ve <b>Enter</b>\'a basın.', ''],
      ['', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
