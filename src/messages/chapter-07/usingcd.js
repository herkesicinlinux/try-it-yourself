window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Accessing Directories at the Command Prompt',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Locate the application titled <code>gcc</code> using <code>whereis</code> command.',
          'Display the present working directory.',
          'Change the current working directory to <code>/usr/bin</code> directory.',
          'Change the current working directory to <code>$HOME</code> directory.',
          'Move to the parent directory.',
          'Go to the previous directory by the shortcut method i.e using <code>-</code> operator.',
          'Display the present working directory.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Please type "whereis gcc" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Proceed and view the present working directory.',
      ],
      [
        'Please type the "pwd" command and press <b>Enter</b>.',
        'You have successfully completed the assigned task. You can now change the current working directory to /usr/bin directory.',
      ],
      [
        'Please type the "cd /usr/bin" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Now, you can change the current working directory to $HOME.',
      ],
      [
        'Please type the "cd" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. You can change the present working directory to the parent directory.',
      ],
      [
        'Please type the "cd .." and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Now, you can change the present working directory to the previous directory by the shortcut method i.e using <code>-</code> operator.',
      ],
      [
        'Please type the "cd -" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. You can now view the present working directory.',
      ],
      ['Please type the "pwd" and press <b>Enter</b>.', ''],
      ['', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: Dizinlere Komut İsteminden Erişme',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          '<code>whereis</code> komutunu kullanarak <code>gcc</code> adlı uygulamayı bulun.',
          'Mevcut çalışma dizinini görüntüleyin.',
          'Mevcut çalışma dizinini <code>/usr/bin</code> dizinine değiştirin.',
          'Mevcut çalışma dizinini <code>$HOME</code> dizini olarak değiştirin.',
          'Ana dizine gidin.',
          'Kısayol yöntemiyle, yani <code>-</code> operatörünü kullanarak önceki dizine gidin.',
          'Mevcut çalışma dizinini görüntüleyin.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Lütfen "whereis gcc" yazın ve <b>Enter</b>\' a basın.',
        'Size verilen görevi başarıyla tamamladınız. Devam edin ve mevcut çalışma dizinini görüntüleyin.',
      ],
      [
        'Lütfen "pwd" komutunu yazın ve <b>Enter</b> tuşuna basın.',
        'Size verilen görevi başarıyla tamamladınız. Artık mevcut çalışma dizinini /usr/bin dizinine değiştirebilirsiniz.',
      ],
      [
        'Lütfen "cd /usr/bin" yazın ve <b>Enter</b> tuşuna basın.',
        'Size verilen görevi başarıyla tamamladınız. Şimdi, mevcut çalışma dizinini $HOME olarak değiştirebilirsiniz. ',
      ],
      [
        'Lütfen "cd" yazın ve <b>Enter</b> tuşuna basın.',
        'Size verilen görevi başarıyla tamamladınız. Mevcut çalışma dizinini üst dizine değiştirebilirsiniz. ',
      ],
      [
        'Lütfen "cd .." yazın ve <b>Enter</b> tuşuna basın.',
        'Size verilen görevi başarıyla tamamladınız. Artık mevcut çalışma dizinini, kısayol yöntemiyle, yani <code>-</code> operatörünü kullanarak önceki dizine değiştirebilirsiniz.',
      ],
      [
        'Lütfen "cd -" yazın ve <b>Enter</b> tuşuna basın.',
        'Size verilen görevi başarıyla tamamladınız. Artık mevcut çalışma dizinini görüntüleyebilirsiniz.',
      ],
      ['Lütfen "pwd" yazın ve <b>Enter</b>\'a basın.', ''],
      ['', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
