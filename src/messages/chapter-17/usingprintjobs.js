window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Managing Print Jobs',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Cancel a print job with job id 11.',
          'Get the list of available printers.',
          'Move print job 8 to the epson-new printer.',
          'Check the status of all printers.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Please type "cancel 11" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. You will now get a list of available printers.',
      ],
      [
        'Please type "lpstat -p -d" and press <b>Enter</b>.',
        'You have successfully completed the assigned tasks. You will now move the print job to the epson-new printer.',
      ],
      [
        'Please type "lpmove 8 epson-new" and press <b>Enter</b>.',
        'You have successfully completed the assigned tasks. You will now check the status of all printers.',
      ],
      ['Please type "lpstat -a" and press <b>Enter</b>.', ''],
      ['', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: Yazdırma İşlerini Yönetme',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'İş kimliği 11 olan yazdırma işini iptal edin.',
          'Kullanılabilir yazıcıların listesini alın.',
          "Yazdırma işi 8'i epson-new yazıcısına taşıyın.",
          'Tüm yazıcıların durumunu kontrol edin.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Lütfen "cancel 11" yazın ve <b>Enter</b> tuşuna basın.',
        'Görevi başarıyla tamamladınız. Şimdi kullanılabilir yazıcıların bir listesini alacaksınız.',
      ],
      [
        'Lütfen "lpstat -p -d" yazın ve <b>Enter</b> tuşuna basın.',
        'Görevleri başarıyla tamamladınız. Şimdi yazdırma işini epson-new yazıcısına taşıyacaksınız.',
      ],
      [
        'Lütfen "lpmove 8 epson-new" yazın ve <b>Enter</b> tuşuna basın.',
        'Görevleri başarıyla tamamladınız. Şimdi tüm yazıcıların durumunu kontrol edeceksiniz.',
      ],
      ['Lütfen "lpstat -a" yazın ve <b>Enter</b>\'a basın.', ''],
      ['', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
