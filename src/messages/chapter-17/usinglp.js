window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Printing With <code>lp</code>',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Send <b><code>testfile.pdf</code></b> to the default printer (a Brother HLL2380DW).',
          'Send <b><code>testfile.pdf</code></b> to the hp-laserjet printer.',
          'Print two copies of <b><code>testfile.pdf</code></b> on the default printer.',
          'Set the hp-laserjet as the default printer.',
          'Check the print queue status.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Please type "lp testfile.pdf" and press the <b>Enter</b> key.',
        'You have successfully completed the assigned task. You will now print the testfile.pdf file to an hp-laserjet printer.',
      ],
      [
        'Please type "lp -d hp-laserjet testfile.pdf" and press the <b>Enter</b> key.',
        'You have successfully completed the assigned task. You will now print two copies of the testfile.pdf file at the default printer.',
      ],
      [
        'Please type "lp -n 2 testfile.pdf" and press the <b>Enter</b> key.',
        'You have successfully completed the assigned task. You will now set the hp-laserjet as the default printer.',
      ],
      [
        'Please type "lpoptions -d hp-laserjet" and press the <b>Enter</b> key.',
        'You have successfully completed the assigned task. You will now check the queue status.',
      ],
      ['Please type "lpq -a" and press the <b>Enter</b> key.', ''],
      ['', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: <code>lp</code> ile Yazdırma',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          '<b><code>testfile.pdf</code></b> dosyasını varsayılan yazıcıya (Brother HLL2380DW) gönderin.',
          '<b><code>testfile.pdf</code></b> dosyasını hp-laserjet yazıcısına gönderin.',
          'Varsayılan yazıcıda <b><code>testfile.pdf</code></b> dosyasının iki kopyasını yazdırın.',
          "hp-laserjet'i varsayılan yazıcı olarak ayarlayın.",
          'Yazdırma kuyruğu durumunu kontrol edin.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Lütfen "lp testfile.pdf" yazın ve <b>Enter</b> tuşuna basın.',
        'Görevi başarıyla tamamladınız. Şimdi testfile.pdf dosyasını hp-laserjet yazıcısına yazdıracaksınız.',
      ],
      [
        'Lütfen "lp -d hp-laserjet testfile.pdf" yazın ve <b>Enter</b> tuşuna basın.',
        'Görevi başarıyla tamamladınız. Şimdi varsayılan yazıcıda testfile.pdf dosyasının iki kopyasını yazdıracaksınız.',
      ],
      [
        'Lütfen "lp -n 2 testfile.pdf" yazın ve <b>Enter</b> tuşuna basın.',
        "Görevi başarıyla tamamladınız. Şimdi hp-laserjet'i varsayılan yazıcı olarak ayarlayacaksınız.",
      ],
      [
        'Lütfen "lpoptions -d hp-laserjet" yazın ve <b>Enter</b> tuşuna basın.',
        'Görevi başarıyla tamamladınız. Şimdi yazdırma kuyruğu durumunu kontrol edeceksiniz.',
      ],
      ['Lütfen "lpq -a" yazın ve <b>Enter</b> tuşuna basın.', ''],
      ['', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
