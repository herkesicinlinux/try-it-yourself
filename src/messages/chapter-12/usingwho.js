window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Identify the Currently Logged-In User and Your User Name',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: ['View the currently Logged-In users.', 'View your user name.'],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Please type "who" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Now you will view the user ID of the currently Logged-In user.',
      ],
      ['Please type "whoami" and press <b>Enter</b>.', ''],
      ['', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: Şu Anda Oturum Açmış Kullanıcıyı ve Kullanıcı Adınızı Belirleyin',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: ['Oturum açmış kullanıcıları görüntüleyin.', 'Kullanıcı adınızı görüntüleyin.'],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Lütfen "who" yazın ve <b>Enter</b>\'a basın.',
        'Görevi başarıyla tamamladınız. Şimdi, oturum açmış kullanıcının kullanıcı kimliğini (ID) görüntüleyeceksiniz.',
      ],
      ['Lütfen "whoami" yazın ve <b>Enter</b>\'a basın.', ''],
      ['', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
