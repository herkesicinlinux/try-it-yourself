window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Using File Permission',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'View the files present in the current directory in the long listing format.',
          'By default the permissions for a file will be <b>rw-rw-r--</b>, change the permissions of the <b>sample2.sh</b> file to <b>rwxr--r-x</b>, using the <b>rwx</b> notation.',
          'View the files present in the current directory in the long listing format.',
          'By default the permissions for a file will be <b>rw-rw-r--</b>, change the permissions of the <b>script1.sh</b> file to <b>rwxr-x--x</b>, using <b>421</b> method.',
          'View the files present in the current directory in the long listing format.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Please type "ls -l" and press <b>Enter</b>.',
        'You have viewed the files present in the current directory. Now change the file permissions of sample2.sh.',
      ],
      [
        'Please type "chmod uo+x,g-w sample2.sh" and press <b>Enter</b>.',
        'You have reset the permissions of sample2.sh. Confirm the same by listing the files in long listing format.',
      ],
      [
        'Please type "ls -l" and press <b>Enter</b>.',
        'You have viewed the files present in the current directory. Now change the file permissions of script1.sh.',
      ],
      [
        'Please type "chmod 751 script1.sh" and press <b>Enter</b>.',
        'You have changed the permissions of the script1.sh. Confirm the same by listing the files in long listing format.',
      ],
      ['Please type "ls -l" and press <b>Enter</b>.', ''],
      ['', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: Dosya İzinlerini Kullanma',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Geçerli dizinde bulunan dosyaları uzun liste biçiminde görüntüleyin.',
          'Varsayılan olarak bir dosyanın izinleri <b>rw-rw-r--</b> olacaktır, <b>sample2.sh</b> dosyasının izinlerini, <b>rwx</b> gösterimini kullanarak <b>rwxr--r-x</b> olarak değiştirin.',
          'Geçerli dizinde bulunan dosyaları uzun liste biçiminde görüntüleyin.',
          'Varsayılan olarak bir dosyanın izinleri <b>rw-rw-r--</b> olacaktır, <b>script1.sh</b> dosyasının izinlerini, <b>421</b> yöntemini kullanarak <b>rwxr-x--x</b> olarak değiştirin.',
          'Geçerli dizinde bulunan dosyaları uzun liste biçiminde görüntüleyin.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Lütfen "ls -l" yazın ve <b>Enter</b> tuşuna basın.',
        'Mevcut dizinde bulunan dosyaları görüntülediniz. Şimdi sample2.sh dosya izinlerini değiştirin.',
      ],
      [
        'Lütfen "chmod uo+x,g-w sample2.sh" yazın ve <b>Enter</b>\'a basın.',
        "sample2.sh'nin izinlerini sıfırladınız. Dosyaları uzun liste biçiminde listeleyerek bunu doğrulayın.",
      ],
      [
        'Lütfen "ls -l" yazın ve <b>Enter</b> tuşuna basın.',
        "Mevcut dizinde bulunan dosyaları görüntülediniz. Şimdi script1.sh'nin dosya izinlerini değiştirin.",
      ],
      [
        'Lütfen "chmod 751 script1.sh" yazın ve <b>Enter</b> tuşuna basın.',
        "script1.sh'nin izinlerini değiştirdiniz. Dosyaları uzun liste biçiminde listeleyerek bunu doğrulayın.",
      ],
      ['Lütfen "ls -l" yazın ve <b>Enter</b> tuşuna basın.', ''],
      ['', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
