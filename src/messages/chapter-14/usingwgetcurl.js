window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Using <code>wget</code> and <code>curl</code>',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Download the FAQ from the <b>Linux Foundation</b> website <a href="http://www.linuxfoundation.org/about/faq">(http://www.linuxfoundation.org/about/faq)</a> using <b>wget</b>.',
          'Read information about <a href="https://lwn.net/">https://lwn.net</a> using <b>curl</b> and place the output in a file named <code><b>lwn.out</b></code>.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Please type "wget http://www.linuxfoundation.org/about/faq" and press <b>Enter</b>.',
        'You have successfully completed the task. Next, you will type in the command to read information about a URL.',
      ],
      ['Please type "curl -o lwn.out https://lwn.net" and press <b>Enter</b>.', ''],
      ['', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: <code>wget</code> ve <code>curl</code> Kullanımı',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          '<b>wget</b> kullanarak, <b>Linux Vakfı</b> web sitesinden SSS sayfasını indirin: <a href="http://www.linuxfoundation.org/about/faq">http://www.linuxfoundation.org/about/faq</a>.',
          '<b>curl</b> kullanarak, <a href="https://lwn.net/">https://lwn.net</a> hakkındaki bilgileri okuyun ve çıktıyı <code><b>lwn.out</b></code> adlı bir dosyaya yerleştirin.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Lütfen "wget http://www.linuxfoundation.org/about/faq" yazın ve <b>Enter</b>\'a basın.',
        'Görevi başarıyla tamamladınız. Ardından, bir URL hakkındaki bilgileri okumak için gerekli komutu yazacaksınız.',
      ],
      ['Lütfen "curl -o lwn.out https://lwn.net" yazın ve <b>Enter</b>\'a basın.', ''],
      ['', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
