window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title:
        'Try-It-Yourself: Using <code>ping</code>, <code>route</code>, and <code>traceroute</code>',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Confirm that the remote host (google.com) is online and is responding.',
          'View the current routing table.',
          'Check the route which the data packet takes to reach the destination host (google.com).',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Please type "ping google.com" and press <b>Enter</b>."',
        'You have successfully completed the task. You will now type in the command to view the current routing table.',
      ],
      [
        'Please type "route -n" and press <b>Enter</b>.',
        'You have successfully completed the task. Next you will type in the command to print the packet route.',
      ],
      ['Please type "traceroute google.com" and press <b>Enter</b>.', ''],
      ['', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title:
        'Kendin Dene: <code>ping</code>, <code>route</code> ve <code>traceroute</code> Kullanımı',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Uzak bilgisayarın (google.com) çevrimiçi olduğunu ve yanıt verdiğini doğrulayın.',
          'Mevcut yönlendirme tablosunu (routing table) görüntüleyin.',
          'Veri paketinin hedef ana bilgisayara (google.com) ulaşmak için izlediği yolu kontrol edin.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Lütfen "ping google.com" yazın ve <b>Enter</b>\'a basın.',
        'Görevi başarıyla tamamladınız. Şimdi, mevcut yönlendirme tablosunu görüntülemek için gerekli komutu yazacaksınız.',
      ],
      [
        'Lütfen "route -n" yazın ve <b>Enter</b> tuşuna basın.',
        'Görevi başarıyla tamamladınız. Ardından, paket yolunu yazdırmak için gerekli komutu yazacaksınız.',
      ],
      ['Lütfen "traceroute google.com" yazın ve <b>Enter</b>\'a basın.', ''],
      ['', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
