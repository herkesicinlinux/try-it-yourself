window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Connecting to an FTP server',
      subtitle:
        'Tasks to be performed: Connecting to an FTP server using a command line <b>ftp</b> client',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Connect to an anonymous FTP site (<a href="ftp://ftp.gnu.org/">ftp.gnu.org</a>) using the command line <b>ftp</b> client.<br />Hint: The user name for anonymous FTP site is <code><b>anonymous</b></code>. (Note: most anonymous FTP sites require you to supply a password, but it does not matter what you give!)',
          'Receive the file <code>welcome.msg</code> from the FTP server using the <code>get</code> command.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      ['Please type "ftp ftp.gnu.org" and press <b>Enter</b>.', ''],
      ['Please type "anonymous" and press <b>Enter</b>.', ''],
      ['Please type "get welcome.msg" and press <b>Enter</b>.', ''],
      ['', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: Bir FTP Sunucusuna Bağlanma',
      subtitle:
        'Gerçekleştirilecek görevler: Komut satırı <b>ftp</b> istemcisi kullanarak bir FTP sunucusuna bağlanma',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Komut satırı <b>ftp</b> istemcisi kullanarak, anonim bir FTP sitesine (<a href="ftp://ftp.gnu.org/">ftp.gnu.org</a>) bağlanın.<br />İpucu: Anonim FTP sitesi için kullanıcı adı <code><b>anonymous</b></code>. (Not: Birçok anonim FTP sitesi, bir parola vermenizi gerektirir, ancak ne verdiğiniz önemli değildir!) ',
          '<code>get</code> komutunu kullanarak FTP sunucusundan <code>welcome.msg</code> dosyasını alın.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      ['Lütfen "ftp ftp.gnu.org" yazın ve <b>Enter</b>\'a basın.', ''],
      ['Lütfen "anonymous" yazın ve <b>Enter</b>\'a basın.', ''],
      ['Lütfen "get welcome.msg" yazın ve <b>Enter</b>\'a basın.', ''],
      ['', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
