window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Using Domain Name System (DNS) and Name Resolution Tools',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Display the contents of <code>/etc/hosts</code> file.',
          'Lookup the hostname for linuxfoundation.org using DNS using <code>host</code>.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Please type "cat /etc/hosts" and press <b>Enter</b>.',
        '',
        'You have successfully completed the assigned task. You will now lookup the hostname for linuxfoundation.org using DNS using host.',
      ],
      ['Please type "host linuxfoundation.org" and press <b>Enter</b>.', '', ''],
      ['', '', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: Etki Alan Adı Sistemi (DNS) ve Ad Çözünürlük Araçları Kullanımı',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          '<code>/etc/hosts</code> dosyasının içeriğini görüntüleyin.',
          '<code>host</code> kullanarak linuxfoundation.org için ana bilgisayar adını (hostname) arayın.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Lütfen "cat /etc/hosts" yazın ve <b>Enter</b> tuşuna basın.',
        '',
        'Görevi başarıyla tamamladınız. Şimdi, host kullanarak linuxfoundation.org için ana bilgisayar adını arayacaksınız.',
      ],
      ['Lütfen "host linuxfoundation.org" yazın ve <b>Enter</b>\'a basın.', '', ''],
      ['', '', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
