window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Using Network Tools',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Use <code>ethtool</code> to show information about the first network device <code>eth0</code>.',
          'Monitor network traffic in text mode using <code>netstat</code>.',
        ],
      },
      extra: 'Use password: <b>student</b>, whenever required.',
    },
    feedbacks: [
      [
        'Please type "sudo ethtool eth0" and press <b>Enter</b>.',
        '',
        'You have successfully completed the assigned task. Please type <b>student</b> and press <b>Enter</b>.',
      ],
      [
        'Please type <b>student</b> and press <b>Enter</b>.',
        'password',
        'You have successfully completed the assigned task. Now you will monitor network traffic in text mode using netstat.',
      ],
      ['Please type "netstat -r" and press <b>Enter</b>.', '', ''],
      ['', '', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: Ağ Araçlarını Kullanma',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'İlk ağ cihazı <code>eth0</code> hakkındaki bilgileri göstermek için <code>ethtool</code> kullanın.',
          '<code>netstat</code> kullanarak metin modunda ağ trafiğini izleyin.',
        ],
      },
      extra: 'Gerektiğinde şifre olarak <b>student</b> kullanın.',
    },
    feedbacks: [
      [
        'Lütfen "sudo ethtool eth0" yazın ve <b>Enter</b> tuşuna basın.',
        '',
        "Görevi başarıyla tamamladınız. Lütfen <b>student</b> yazın ve <b>Enter</b>'a basın.",
      ],
      [
        "Lütfen <b>student</b> yazın ve <b>Enter</b>'a basın.",
        'password',
        'Görevi başarıyla tamamladınız. Şimdi netstat kullanarak metin modunda ağ trafiğini izleyeceksiniz.',
      ],
      ['Lütfen "netstat -r" yazın ve <b>Enter</b>\'a basın.', '', ''],
      ['', '', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
