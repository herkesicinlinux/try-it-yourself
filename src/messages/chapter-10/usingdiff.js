window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Comparing Files',
      subtitle: 'Tasks to be performed:',
      description:
        'In a production environment, you have created a file (<b>MyFile1</b>) with reference to an existing file (<b>MyFile2</b>). Your colleague has also created a file (<b>MyFile3</b>) based on the same existing file.',
      list: {
        type: 'ol',
        items: [
          'Compare the contents of your file and the reference file, using <code>diff</code>.',
          'Compare the differences between the files created by you and your friend with reference to the existing file, using <code>diff3</code>.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Please type "diff MyFile1 MyFile2" and press <b>Enter</b>.',
        'You have successfully completed the assigned task. Now you will compare three files.',
      ],
      ['Please type "diff3 MyFile1 MyFile2 MyFile3" and press <b>Enter</b>.', ''],
      ['', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: Dosyaları Karşılaştırma',
      subtitle: 'Gerçekleştirilecek görevler:',
      description:
        'Bir çalışma ortamında, mevcut bir dosyaya (<b>MyFile2</b>) referans içeren yeni bir dosya (<b>MyFile1</b>) oluşturdunuz. Meslektaşınız da aynı mevcut dosyayı temel alan başka bir dosya (<b>MyFile3</b>) oluşturdu.',
      list: {
        type: 'ol',
        items: [
          'Dosyanızın içeriğini ve referans dosyanızı <code>diff</code> kullanarak karşılaştırın.',
          'Sizin ve arkadaşınızın oluşturduğu dosyalar arasındaki farkları, <code>diff3</code> kullanarak mevcut dosyaya referansla karşılaştırın.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Lütfen "diff MyFile1 MyFile2" yazın ve <b>Enter</ b> tuşuna basın.',
        'Görevi başarıyla tamamladınız. Şimdi üç dosyayı karşılaştıracaksınız.',
      ],
      ['Lütfen "diff3 MyFile1 MyFile2 MyFile3" yazın ve <b>Enter</b>\'a basın.', ''],
      ['', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
