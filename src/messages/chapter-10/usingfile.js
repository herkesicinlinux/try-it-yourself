window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Using <code>file</code>',
      subtitle: 'Tasks to be performed:',
      description: `In a production environment, you have created the following items:
        <ol>
          <li>A shell script named <b>sam1</b>.</li>
          <li>An empty file named <b>new-test1</b>.</li>
          <li>A directory named <b>videos</b>.</li>
        </ol>`,
      list: {
        type: 'ol',
        items: [
          'Tasks to be performed: Find the type of each of the above files/directories, using <code>file</code>.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Please type "file sam1" and press <b>Enter</b>.',
        "You have successfully completed the assigned task. Now you will check the file type for 'new-test1'.",
      ],
      [
        'Please type "file new-test1" and press <b>Enter</b>.',
        "You have successfully completed the assigned task. Now you will check the file type for 'videos'.",
      ],
      ['Please type "file videos" and press <b>Enter</b>.', ''],
      ['', ''],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: 'Kendin Dene: <code>file</code> Kullanımı',
      subtitle: 'Gerçekleştirilecek görevler:',
      description: `Bir çalışma ortamında, aşağıdaki öğeleri oluşturdunuz:
        <ol>
          <li><b>sam1</b> adlı bir kabuk betiği dosyası.</li>
          <li><b>new-test1</b> adlı boş bir dosya.</li>
          <li><b>videos</b> adlı bir dizin.</li>
        </ol>`,
      list: {
        type: 'ol',
        items: [
          'Gerçekleştirilecek görevler: Yukarıdaki dosyaların/dizinlerin her birinin türünü <code>file</code> kullanarak bulun.',
        ],
      },
      extra: '',
    },
    feedbacks: [
      [
        'Lütfen "file sam1" yazın ve <b>Enter</b> tuşuna basın.',
        "Görevi başarıyla tamamladınız. Şimdi 'new-test1' için dosya türünü kontrol edeceksiniz.",
      ],
      [
        'Lütfen "file new-test1" yazın ve <b>Enter</b>\'a basın.',
        "Görevi başarıyla tamamladınız. Şimdi 'videos' için dosya türünü kontrol edeceksiniz.",
      ],
      ['Lütfen "file videos" yazın ve <b>Enter</b>\'a basın.', ''],
      ['', ''],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
