window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Locating and Setting Default Applications in Ubuntu',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: ['Locate the Firefox application.', 'Set Firefox as the default web browser.'],
      },
      extra: '',
    },
    feedbacks: [
      ['Please click the <b>Ubuntu</b> icon on the top-left panel.'],
      ['Please click the <b>Firefox</b> Web Browser.'],
      ['Please click <b>System Settings</b> icon from the left panel.'],
      ['Please select <b>Details</b> in the <b>System Settings</b> window.'],
      ['Please select <b>Default Applications</b> in the <b>Details</b> window.'],
      ['In the <b>Web</b> list, select the <b>Firefox Web Browser</b>.'],
      ['Select the <b>Firefox Web Browser</b>.'],
    ],
    success: [
      'You have successfully located Firefox. You will now set Firefox as the default web browser.',
      'You have successfully set Firefox as your default Web browser.',
    ],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: "Kendin Dene: Ubuntu'da Varsayılan Uygulamaları Bulma ve Ayarlama",
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Firefox uygulamasını bulun.',
          "Firefox'u varsayılan web tarayıcısı olarak ayarlayın.",
        ],
      },
      extra: '',
    },
    feedbacks: [
      ['Lütfen sol üst paneldeki <b>Ubuntu</b> simgesini tıklayın.'],
      ['Lütfen <b>Firefox</b> Web Tarayıcısını tıklayın.'],
      ['Lütfen sol panelden <b>System Settings</b> simgesini tıklayın.'],
      ["Lütfen <b>System Settings</b> penceresinde <b>Details</b>'ı seçin."],
      ["Lütfen <b>Details</b> penceresinde <b>Default Applications</b>'ı seçin."],
      ['<b>Web</b> listesinde, <b>Firefox Web Tarayıcısı</b>nı seçin.'],
      ['<b>Firefox Web Tarayıcısı</b>nı seçin.'],
    ],
    success: [
      "Firefox'u başarıyla konumlandırdınız. Şimdi Firefox'u varsayılan web tarayıcısı olarak ayarlayacaksınız. ",
      "Firefox'u varsayılan Web tarayıcınız olarak başarıyla ayarladınız.",
    ],
  },
};
