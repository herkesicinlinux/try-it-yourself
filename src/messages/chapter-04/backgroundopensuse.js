window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Changing the Desktop Background in openSUSE',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ul',
        items: ['Change the Desktop Background in openSUSE.', 'Use the Theme drop-down list.'],
      },
    },
    feedbacks: [
      ['Right-click anywhere on the desktop.'],
      ['Click <b>Change Background</b>.'],
      ['Click <b>Background</b> to change the desktop background.'],
      ['Select any image from the ones available and click the <b>Select</b> button.'],
    ],
    success: ['You have successfully completed the assigned tasks.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: "Kendin Dene: openSUSE'de Masaüstü Arka Planını Değiştirme",
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ul',
        items: ["OpenSUSE'de Masaüstü Arka Planını değiştirin.", 'Tema açılır listesini kullanın.'],
      },
    },
    feedbacks: [
      ['Masaüstünde herhangi bir yere sağ tıklayın.'],
      ['<b>Change Background</b> öğesine tıklayın.'],
      ["Masaüstü arka planını değiştirmek için <b>Background</b>'ı tıklayın."],
      ['Mevcut görsellerden herhangi birini seçin ve <b>Select</b> butonuna tıklayın.'],
    ],
    success: ['Görevleri başarıyla tamamladınız.'],
  },
};
