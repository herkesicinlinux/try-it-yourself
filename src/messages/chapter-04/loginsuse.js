window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Logging In and Logging Out using the GUI in openSUSE',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: ['Log In.', 'Log Out.'],
      },
      extra: 'Select the username <b>test2</b> and type <b>Torv@Ld5</b>.',
    },
    feedbacks: [
      ['Please select <b>test2</b>.'],
      ['Please type <b>Torv@Ld5</b> in the password box and click <b>Sign In</b>.'],
      ['Please select the power icon on the top-right of the screen.'],
      ['Please select <b>test2</b>.'],
      ['Please click <b>Log Out</b>.'],
      ['Please click the <b>Log Out</b> button.'],
    ],
    success: [
      'You have successfully logged into the system. You will now log off of the system.',
      'You have successfully logged out of the system.',
    ],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: "Kendin Dene: openSUSE'de Grafik Arabirim Kullanarak Oturum Açma ve Oturumu Kapatma",
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: ['Oturum Aç.', 'Oturumu Kapat.'],
      },
      extra: '<b>test2</b> kullanıcısını seçin ve <b>Torv@Ld5</b> yazın.',
    },
    feedbacks: [
      ["Lütfen <b>test2</b>'yi seçin."],
      ['Lütfen parola kutusuna <b>Torv@Ld5</b> yazın ve <b>Sign In</b> butonuna tıklayın.'],
      ['Lütfen ekranın sağ üst köşesindeki güç simgesini tıklayın.'],
      ["Lütfen <b>test2</b>'yi seçin."],
      ["Lütfen <b>Log Out</b>'u seçin."],
      ['Lütfen <b>Log Out</b> butonuna tıklayın.'],
    ],
    success: [
      'Sisteme başarıyla giriş yaptınız. Şimdi sistemden çıkış yapacaksınız.',
      'Sistemden başarıyla çıkış yaptınız.',
    ],
  },
};
