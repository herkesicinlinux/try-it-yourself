window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Locating and Setting Default Applications in openSUSE',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ol',
        items: ['Locate the Firefox application.', 'Set Firefox as the default web browser.'],
      },
      extra: 'Use Activities button from the top panel.',
    },
    feedbacks: [
      ['Please click <b>Activities</b> in the top-left corner of your desktop.'],
      ['Please click the <b>Show Applications</b> icon on the left panel.'],
      ['Please click the <b>Firefox</b> icon.'],
      ['Please click <b>Activities</b> on the top-left corner of your desktop.'],
      ['Please type <b>Details</b> in the search box.'],
      ['Please click the <b>Details</b> icon to open the <b>Details</b> window.'],
      ['Please click <b>Default Applications</b> from the <b>Details</b> window.'],
      ['Please click the <b>Web</b> drop-down list.'],
      ['Please select <b>Firefox</b> to set it as the default web browser.'],
    ],
    success: [
      'You have successfully located Firefox. You will now set Firefox as the default web browser.',
      'You have successfully selected Firefox as the default web browser.',
    ],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: "Kendin Dene: openSUSE'de Varsayılan Uygulamaları Bulma ve Ayarlama",
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: [
          'Firefox uygulamasını bulun.',
          "Firefox'u varsayılan web tarayıcısı olarak ayarlayın.",
        ],
      },
      extra: 'Üst paneldeki Etkinlikler (Activities) butonunu kullanın.',
    },
    feedbacks: [
      ["Lütfen masaüstünüzün sol üst köşesindeki <b>Aktiviteler</b>'i tıklayın. "],
      ['Lütfen sol paneldeki <b>Uygulamaları Göster</b> simgesini tıklayın.'],
      ['Lütfen <b>Firefox</b> simgesini tıklayın.'],
      ["Lütfen masaüstünüzün sol üst köşesindeki <b>Etkinlikler</b>'i tıklayın. "],
      ['Lütfen arama kutusuna <b>Details</b> yazın.'],
      ['Lütfen <b>Details</b> penceresini açmak için <b>Details</b> simgesini tıklayın.'],
      ["Lütfen <b>Details</b> penceresinden <b>Default Applications</b>'ı tıklayın. "],
      ['Lütfen <b>Web</b> açılır listesini tıklayın.'],
      ["Varsayılan web tarayıcısı olarak ayarlamak için lütfen <b>Firefox</b>'u seçin."],
    ],
    success: [
      "Firefox'u başarıyla konumlandırdınız. Şimdi Firefox'u varsayılan web tarayıcısı olarak ayarlayacaksınız. ",
      "Firefox'u varsayılan web tarayıcısı olarak başarıyla seçtiniz.",
    ],
  },
};
