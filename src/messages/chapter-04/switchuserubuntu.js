window.tiy.messages = {
  //
  // English (default)
  //
  en: {
    steps: {
      title: 'Try-It-Yourself: Switching Users in Ubuntu',
      subtitle: 'Tasks to be performed:',
      description: '',
      list: {
        type: 'ul',
        items: [
          "Assume that you have logged in as 'test1'. Switch to 'user1' with password Torv@Ld5.",
        ],
      },
      extra: 'Start from power icon on the right top.',
    },
    feedbacks: [
      ['Please click the power icon in the upper-right corner.'],
      ['Please click <b>user1</b> to switch to the user1 account.'],
      ['Please type <b>Torv@Ld5</b> and press <b>Enter</b>.'],
    ],
    success: ['You have successfully completed the assigned task.'],
  },
  //
  // Turkish
  //
  tr: {
    steps: {
      title: "Kendin Dene: Ubuntu'da Kullanıcı Değiştirme",
      subtitle: 'Gerçekleştirilecek görevler:',
      description: '',
      list: {
        type: 'ol',
        items: ["'test1' olarak giriş yaptığınızı varsayın. Torv@Ld5 parolasıyla 'user1'e geçin."],
      },
      extra: 'Sağ üstteki güç simgesinden başlayın.',
    },
    feedbacks: [
      ['Lütfen sağ üst köşedeki güç simgesini tıklayın.'],
      ["Lütfen user1 hesabına geçmek için <b>user1</b>'i tıklayın."],
      ["Lütfen <b>Torv@Ld5</b> yazın ve <b>Enter</b>'a basın."],
    ],
    success: ['Görevi başarıyla tamamladınız.'],
  },
};
