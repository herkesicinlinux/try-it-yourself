const fs = require('fs');
const path = require('path');
const fetch = require('node-fetch');

const BASE_URL = 'http://linuxfoundation.s3-website-us-east-1.amazonaws.com/TIY';

const fetchImages = (imgs) => {
  const tiyDir = path.dirname(path.resolve('.')).split(path.sep).pop();

  imgs.forEach(async (imgArr) => {
    const [imgName] = imgArr;
    const imgUrl = `${BASE_URL}/${tiyDir}/images/${imgName}`;
    const resp = await fetch(imgUrl);
    const buffer = await resp.buffer();

    const savePath = path.join('..', 'images', imgName);
    fs.writeFile(savePath, buffer, () => {
      console.log(imgUrl, ':done!');
    });
  });
};

fetchImages();
