const fs = require('fs');
const path = require('path');
const MarkdownIt = require('markdown-it');

const md = new MarkdownIt();

const readme = fs.readFileSync(path.join(__dirname, '..', 'README.md')).toString();
const template = fs.readFileSync(path.join(__dirname, 'template.html')).toString();

const rendered = md
  .render(readme)
  .replace(new RegExp('src/', 'g'), '') // remove src/ from links
  .replace(new RegExp('\\[\\s+\\]', 'g'), '<input type="checkbox" disabled />') // handle unchecked checkboxes
  .replace(new RegExp('\\[[x|o]\\]', 'gi'), '<input type="checkbox" disabled checked />'); // handle checked checkboxes

const finalStr = template.replace('CONTENT_AREA', rendered);

fs.writeFileSync(path.join(__dirname, '..', 'index.html'), finalStr);
