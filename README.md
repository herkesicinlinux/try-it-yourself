# Try It Yourself

Try-It-Yourself exercises for LFS101.

## Parameters

| Param        | Default | Description                                                    | Usage                                                                   |
| ------------ | ------- | -------------------------------------------------------------- | ----------------------------------------------------------------------- |
| `lang`       | `en`    | Set the page language to one of the supported langs            | `?lang=LANG` if no other params are present, `&lang=LANG` otherwise     |
| `langswitch` | `false` | Displays the language switcher at the bottom right corner      | `?langswitch` if no other params are present, `&langswitch` otherwise   |
| `nofocus`    | `false` | Disables the auto-focus behaviour found on cmd-style exercises | `?nofocus` if no other params are present, `&nofocus` otherwise         |
| `theme`      | `light` | Changes the theme                                              | `?theme=THEME` if no other params are present, `&theme=THEME` otherwise |

## Contents

- Chapter 3

  - Viewing the Filesystem Hierarchy from the Graphical Interface in Ubuntu
    - [x] [English](src/contents/chapter-03/fileexploreubuntu/index.html)
    - [x] [Turkish](src/contents/chapter-03/fileexploreubuntu/index.html?lang=tr)

- Chapter 4

  - Changing the Desktop Background in openSUSE
    - [x] [English](src/contents/chapter-04/backgroundopensuse/index.html)
    - [x] [Turkish](src/contents/chapter-04/backgroundopensuse/index.html?lang=tr)
  - Logging In and Logging Out using the GUI in openSUSE
    - [x] [English](src/contents/chapter-04/loginsuse/index.html)
    - [x] [Turkish](src/contents/chapter-04/loginsuse/index.html?lang=tr)
  - Switching Users in Ubuntu
    - [x] [English](src/contents/chapter-04/switchuserubuntu/index.html)
    - [x] [Turkish](src/contents/chapter-04/switchuserubuntu/index.html?lang=tr)
  - Locating and Setting Default Applications in Ubuntu
    - [x] [English](src/contents/chapter-04/usingdefubuntu/index.html)
    - [x] [Turkish](src/contents/chapter-04/usingdefubuntu/index.html?lang=tr)
  - Locating and Setting Default Applications in openSUSE
    - [x] [English](src/contents/chapter-04/usingdefsuse/index.html)
    - [x] [Turkish](src/contents/chapter-04/usingdefsuse/index.html?lang=tr)

- Chapter 5

  - Applying System and Display Settings in Ubuntu
    - [x] [English](src/contents/chapter-05/setdisplayubuntu/index.html)
    - [x] [Turkish](src/contents/chapter-05/setdisplayubuntu/index.html?lang=tr)

- Chapter 7

  - Accessing Directories at the Command Prompt
    - [x] [English](src/contents/chapter-07/usingcd/index.html)
    - [x] [Turkish](src/contents/chapter-07/usingcd/index.html?lang=tr)
  - Working with Files and Directories at the Command Prompt
    - [x] [English](src/contents/chapter-07/usingfilesdirs/index.html)
    - [x] [Turkish](src/contents/chapter-07/usingfilesdirs/index.html?lang=tr)
  - Locating Files
    - [x] [English](src/contents/chapter-07/usinglocate/index.html)
    - [x] [Turkish](src/contents/chapter-07/usinglocate/index.html?lang=tr)
  - Using bash Wildcards with ls
    - [x] [English](src/contents/chapter-07/usinglswildcards/index.html)
    - [x] [Turkish](src/contents/chapter-07/usinglswildcards/index.html?lang=tr)
  - Finding Files in a Directory
    - [x] [English](src/contents/chapter-07/usingfind/index.html)
    - [x] [Turkish](src/contents/chapter-07/usingfind/index.html?lang=tr)

- Chapter 10

  - Comparing Files
    - [x] [English](src/contents/chapter-10/usingdiff/index.html)
    - [x] [Turkish](src/contents/chapter-10/usingdiff/index.html?lang=tr)
  - Using file
    - [x] [English](src/contents/chapter-10/usingfile/index.html)
    - [x] [Turkish](src/contents/chapter-10/usingfile/index.html?lang=tr)

- Chapter 12

  - Identify the Currently Logged-In User and your user name
    - [x] [English](src/contents/chapter-12/usingwho/index.html)
    - [x] [Turkish](src/contents/chapter-12/usingwho/index.html?lang=tr)
  - Using chmod to Change File Permissions
    - [x] [English](src/contents/chapter-12/usingchmod/index.html)
    - [x] [Turkish](src/contents/chapter-12/usingchmod/index.html?lang=tr)

- Chapter 13

  - Using cat
    - [x] [English](src/contents/chapter-13/usingcat/index.html)
    - [x] [Turkish](src/contents/chapter-13/usingcat/index.html?lang=tr)
  - Using echo
    - [x] [English](src/contents/chapter-13/usingecho/index.html)
    - [x] [Turkish](src/contents/chapter-13/usingecho/index.html?lang=tr)
  - Using head and tail
    - [x] [English](src/contents/chapter-13/usingheadtail/index.html)
    - [x] [Turkish](src/contents/chapter-13/usingheadtail/index.html?lang=tr)
  - Using sort and uniq
    - [x] [English](src/contents/chapter-13/usingsort/index.html)
    - [x] [Turkish](src/contents/chapter-13/usingsort/index.html?lang=tr)
  - Using tr
    - [x] [English](src/contents/chapter-13/usingtr/index.html)
    - [x] [Turkish](src/contents/chapter-13/usingtr/index.html?lang=tr)
  - Using wc
    - [x] [English](src/contents/chapter-13/usingwc/index.html)
    - [x] [Turkish](src/contents/chapter-13/usingwc/index.html?lang=tr)

- Chapter 14

  - Using Domain Name System (DNS) and Name Resolution Tools
    - [x] [English](src/contents/chapter-14/usinghost/index.html)
    - [x] [Turkish](src/contents/chapter-14/usinghost/index.html?lang=tr)
  - Using ping, route, and traceroute
    - [x] [English](src/contents/chapter-14/usingping/index.html)
    - [x] [Turkish](src/contents/chapter-14/usingping/index.html?lang=tr)
  - Using Network Tools
    - [x] [English](src/contents/chapter-14/usingethtool/index.html)
    - [x] [Turkish](src/contents/chapter-14/usingethtool/index.html?lang=tr)
  - Using wget and curl
    - [x] [English](src/contents/chapter-14/usingwgetcurl/index.html)
    - [x] [Turkish](src/contents/chapter-14/usingwgetcurl/index.html?lang=tr)
  - Connecting to an FTP server
    - [x] [English](src/contents/chapter-14/usingftp/index.html)
    - [x] [Turkish](src/contents/chapter-14/usingftp/index.html?lang=tr)

- Chapter 17

  - Printing With lp
    - [x] [English](src/contents/chapter-17/usinglp/index.html)
    - [x] [Turkish](src/contents/chapter-17/usinglp/index.html?lang=tr)
  - Managing Print Jobs
    - [x] [English](src/contents/chapter-17/usingprintjobs/index.html)
    - [x] [Turkish](src/contents/chapter-17/usingprintjobs/index.html?lang=tr)

## License

[![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)

This work is a partial fork of [Introduction to Linux](https://www.edx.org/course/introduction-to-linux), originally appeared on edX.org (licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)), and licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
